<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Tests\Unit\Models\Parser;

class FloatColumnParserTest extends ColumnParserTestCase
{
    public function dataTypes(): array
    {
        return [
            'decimal'               => [
                'definition'   => 'decimal',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'decimal',
                    'dataTypeCast' => 'float',
                    'precision'    => 8,
                    'scale'        => 2,
                    'warningCount' => 2,
                    'errorCount'   => 0,
                    'warnings'     => [
                        'Missing precision. Set to default: 8',
                        'Missing scale. Set to default: 2',
                    ],
                    'errors'       => [],
                ]),
            ],
            'decimal-10-4'          => [
                'definition'   => 'decimal|precision:10|scale:4',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'decimal',
                    'dataTypeCast' => 'float',
                    'precision'    => 10,
                    'scale'        => 4,
                    'warningCount' => 0,
                    'errorCount'   => 0,
                    'warnings'     => [],
                    'errors'       => [],
                ]),
            ],
            'decimal-10-4-unsigned' => [
                'definition'   => 'decimal|precision:10|scale:4|unsigned',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'decimal',
                    'dataTypeCast' => 'float',
                    'unsigned'     => true,
                    'precision'    => 10,
                    'scale'        => 4,
                    'warningCount' => 0,
                    'errorCount'   => 0,
                    'warnings'     => [],
                    'errors'       => [],
                ]),
            ],
        ];
    }

    public function dataTypeAliases(): array
    {
        return [];
    }
}
