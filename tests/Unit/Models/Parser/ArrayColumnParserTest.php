<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Tests\Unit\Models\Parser;

class ArrayColumnParserTest extends ColumnParserTestCase
{
    public function dataTypes(): array
    {
        return [
            'json' => [
                'definition'   => 'json',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'json',
                    'dataTypeCast' => 'array',
                ]),
            ],
        ];
    }

    public function dataTypeAliases(): array
    {
        return [];
    }
}
