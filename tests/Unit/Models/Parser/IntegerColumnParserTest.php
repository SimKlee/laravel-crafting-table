<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Tests\Unit\Models\Parser;

class IntegerColumnParserTest extends ColumnParserTestCase
{
    public function dataTypes(): array
    {
        return [
            'tinyInteger'               => [
                'definition'   => 'tinyInteger',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'tinyInteger']),
            ],
            'smallInteger'              => [
                'definition'   => 'smallInteger',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'smallInteger']),
            ],
            'mediumInteger'             => [
                'definition'   => 'mediumInteger',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'mediumInteger']),
            ],
            'integer'                   => [
                'definition'   => 'integer',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'integer']),
            ],
            'bigInteger'                => [
                'definition'   => 'bigInteger',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'bigInteger']),
            ],
            'integer-nullable'          => [
                'definition'   => 'integer|nullable',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType' => 'integer',
                    'nullable' => true,
                ]),
            ],
            'integer-unsigned-nullable' => [
                'definition'   => 'integer|unsigned|nullable',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType' => 'integer',
                    'unsigned' => true,
                    'nullable' => true,
                ]),
            ],
            'integer-unsigned-ai'       => [
                'definition'   => 'integer|unsigned|ai',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'      => 'integer',
                    'unsigned'      => true,
                    'autoIncrement' => true,
                ]),
            ],
        ];
    }

    public function dataTypeAliases(): array
    {
        return [
            'alias-tinyint'   => [
                'definition'   => 'tinyint',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'tinyInteger']),
            ],
            'alias-tinyInt'   => [
                'definition'   => 'tinyInt',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'tinyInteger']),
            ],
            'alias-TINYINT'   => [
                'definition'   => 'TINYINT',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'tinyInteger']),
            ],
            'alias-TinYinT'   => [
                'definition'   => 'TinYinT',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'tinyInteger']),
            ],
            'alias-smallint'  => [
                'definition'   => 'smallint',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'smallInteger']),
            ],
            'alias-mediumint' => [
                'definition'   => 'mediumint',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'mediumInteger']),
            ],
            'alias-int'       => [
                'definition'   => 'int',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'integer']),
            ],
            'alias-bigint'    => [
                'definition'   => 'bigint',
                'expectations' => $this->getMergedExpectationFromDefaults(['dataType' => 'bigInteger']),
            ],
        ];
    }
}
