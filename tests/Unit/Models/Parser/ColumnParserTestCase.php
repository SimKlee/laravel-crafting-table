<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Tests\Unit\Models\Parser;

use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;
use SimKlee\LaravelCraftingTable\Models\Parser\ColumnParser;
use SimKlee\LaravelCraftingTable\Tests\Unit\UnitTestCase;

abstract class ColumnParserTestCase extends UnitTestCase
{
    abstract public function dataTypes(): array;

    abstract public function dataTypeAliases(): array;

    /**
     * @dataProvider dataTypes
     * @dataProvider dataTypeAliases
     */
    public function testDataType(string $definition, array $expectations): void
    {
        $this->runDataTypeTest($definition, $expectations);
    }

    protected function getMergedExpectationFromDefaults(array $expectations): array
    {
        $columnDefinition = new ColumnDefinition();

        return array_merge([
            'unsigned'      => $columnDefinition->unsigned,
            'nullable'      => $columnDefinition->nullable,
            'foreignKey'    => $columnDefinition->foreignKey,
            'autoIncrement' => $columnDefinition->autoIncrement,
            'length'        => $columnDefinition->length,
            'precision'     => $columnDefinition->precision,
            'scale'         => $columnDefinition->scale,
            'default'       => $columnDefinition->default,
            'warningCount'  => 0,
            'errorCount'    => 0,
            'warnings'      => [],
            'errors'        => [],
        ], $expectations);
    }

    protected function runDataTypeTest(string $definition, array $expectations): void
    {
        $parser           = new ColumnParser(name: 'test_column', definition: $definition);
        $columnDefinition = $parser->getColumnDefinition();
        $this->assertInstanceOf(expected: ColumnDefinition::class, actual: $columnDefinition);
        $this->assertSameIfExpected($columnDefinition, $expectations, 'dataType');
        $this->assertSameIfExpected($columnDefinition, $expectations, 'dataTypeCast');
        $this->assertSameIfExpected($columnDefinition, $expectations, 'unsigned');
        $this->assertSameIfExpected($columnDefinition, $expectations, 'nullable');
        $this->assertSameIfExpected($columnDefinition, $expectations, 'length');
        $this->assertSameIfExpected($columnDefinition, $expectations, 'precision');
        $this->assertSameIfExpected($columnDefinition, $expectations, 'decimals');
        $this->assertWarningAndErrorCount($parser, $expectations);
    }

    protected function assertSameIfExpected(ColumnDefinition $columnDefinition, array $expectations, string $key): void
    {
        if (isset($expectations[$key])) {
            $this->assertSame(expected: $expectations[$key], actual: $columnDefinition->{$key}, message: $key);
        }
    }

    protected function assertWarningAndErrorCount(ColumnParser $parser, array $expectations): void
    {
        if (isset($expectations['warningCount'])) {
            $this->assertCount($expectations['warningCount'], $parser->getWarnings(), 'warning count: ' . print_r($parser->getWarnings()->toArray(), true));
            if ($expectations['warningCount'] > 0) {
                $warnings = $parser->getWarnings();
                foreach ($expectations['warnings'] as $warning) {
                    $this->assertTrue($warnings->contains($warning), 'MISSING WARNING: ' . $warning);
                }
            }
        }

        if (isset($expectations['errorCount'])) {
            $this->assertCount($expectations['errorCount'], $parser->getErrors(), 'error count' . print_r($parser->getErrors()->toArray(), true));
            if ($expectations['errorCount'] > 0) {
                $errors = $parser->getErrors();
                foreach ($expectations['errors'] as $error) {
                    $this->assertTrue($errors->contains($error), 'MISSING ERROR: ' . $error);
                }
            }
        }
    }
}
