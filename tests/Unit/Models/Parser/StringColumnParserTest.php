<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Tests\Unit\Models\Parser;

class StringColumnParserTest extends ColumnParserTestCase
{
    public function dataTypes(): array
    {
        return [
            'string'                         => [
                'definition'   => 'string',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'string',
                    'dataTypeCast' => 'string',
                    'errorCount'   => 1,
                    'errors'       => [
                        'Missing length. No default value defined.',
                    ],
                ]),
            ],
            'string-25'                      => [
                'definition'   => 'string|length:25',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'string',
                    'dataTypeCast' => 'string',
                    'length'       => 25,
                ]),
            ],
            'string-nullable'                => [
                'definition'   => 'string|nullable',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'string',
                    'dataTypeCast' => 'string',
                    'nullable'     => true,
                    'errorCount'   => 1,
                    'errors'       => [
                        'Missing length. No default value defined.',
                    ],
                ]),
            ],
            'string-25-nullable'             => [
                'definition'   => 'string|length:25|nullable',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'string',
                    'dataTypeCast' => 'string',
                    'nullable'     => true,
                    'length'       => 25,
                ]),
            ],
            'string-25-nullable-default-abc' => [
                'definition'   => 'string|length:25|default:abc',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'string',
                    'dataTypeCast' => 'string',
                    'nullable'     => false,
                    'length'       => 25,
                    'default'      => 'abc',
                ]),
            ],
            'string-25-nullable-default-null' => [
                'definition'   => 'string|length:25|default:Null',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'string',
                    'dataTypeCast' => 'string',
                    'nullable'     => false,
                    'length'       => 25,
                    'default'      => 'NULL',
                ]),
            ],
            'char'                           => [
                'definition'   => 'char',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'char',
                    'dataTypeCast' => 'string',
                    'errorCount'   => 1,
                    'errors'       => [
                        'Missing length. No default value defined.',
                    ],
                ]),
            ],
            'char-25'                        => [
                'definition'   => 'char|length:25',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'char',
                    'dataTypeCast' => 'string',
                    'length'       => 25,
                ]),
            ],
            'char-nullable'                  => [
                'definition'   => 'char|nullable',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'char',
                    'dataTypeCast' => 'string',
                    'nullable'     => true,
                    'errorCount'   => 1,
                    'errors'       => [
                        'Missing length. No default value defined.',
                    ],
                ]),
            ],
            'char-25-nullable'               => [
                'definition'   => 'char|length:25|nullable',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'char',
                    'dataTypeCast' => 'string',
                    'nullable'     => true,
                    'length'       => 25,
                ]),
            ],
            'char-25-nullable-default-abc'   => [
                'definition'   => 'char|length:25|default:abc',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'char',
                    'dataTypeCast' => 'string',
                    'nullable'     => false,
                    'length'       => 25,
                    'default'      => 'abc',
                ]),
            ],
            'text'                           => [
                'definition'   => 'text',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'text',
                    'dataTypeCast' => 'string',
                ]),
            ],
        ];
    }

    public function dataTypeAliases(): array
    {
        return [
            'varchar-25' => [
                'definition'   => 'varchar|length:25',
                'expectations' => $this->getMergedExpectationFromDefaults([
                    'dataType'     => 'string',
                    'dataTypeCast' => 'string',
                    'length'       => 25
                ]),
            ],
        ];
    }
}
