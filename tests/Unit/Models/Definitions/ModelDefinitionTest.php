<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions;

use Illuminate\Support\Collection;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition;
use SimKlee\LaravelCraftingTable\Tests\Unit\UnitTestCase;

class ModelDefinitionTest extends UnitTestCase
{
    public function testConfigDefaultValues()
    {
        $modelDefinition = new ModelDefinition('TestModel', []);
        $this->assertSame(expected: 'TestModel', actual: $modelDefinition->model);
        $this->assertInstanceOf(expected: Collection::class, actual: $modelDefinition->columns);
        $this->assertSame(expected: [], actual: $modelDefinition->values);
        $this->assertSame(expected: [], actual: $modelDefinition->defaults);
        $this->assertFalse(condition: $modelDefinition->timestamps);
        $this->assertFalse(condition: $modelDefinition->softDelete);
        $this->assertFalse(condition: $modelDefinition->uuid);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testModelNameIsSet(string $model, array $config)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertSame(expected: $model, actual: $modelDefinition->model);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testModelTableNameIsSet(string $model, array $config)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertSame(expected: $config['table'], actual: $modelDefinition->table);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testValuesAreSet(string $model, array $config)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertSame(expected: $config['values'], actual: $modelDefinition->values);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testDefaultsAreSet(string $model, array $config)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertSame(expected: $config['defaults'], actual: $modelDefinition->defaults);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testTimestampsAreSet(string $model, array $config)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertSame(expected: $config['timestamps'], actual: $modelDefinition->timestamps);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testSoftDeleteIsSet(string $model, array $config)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertSame(expected: $config['softDelete'], actual: $modelDefinition->softDelete);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testUuidIsSet(string $model, array $config)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertSame(expected: $config['uuid'], actual: $modelDefinition->uuid);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testHasDates(string $model, array $config, array $expectations)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertSame(expected: $expectations['hasDates'], actual: $modelDefinition->hasDates());
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testGetDatesCount(string $model, array $config, array $expectations)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertCount(expectedCount: $expectations['getDatesCount'], haystack: $modelDefinition->getDates());
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testColumnCount(string $model, array $config, array $expectations)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertCount(expectedCount: $expectations['columnCount'], haystack: $modelDefinition->columns);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::model()
     */
    public function testGetColumnsWithForeignKeyCount(string $model, array $config, array $expectations)
    {
        $modelDefinition = new ModelDefinition($model, $config);
        $this->assertCount(expectedCount: $expectations['columnsWithForeignKeyCount'], haystack: $modelDefinition->getColumnsWithForeignKey());
    }

    public function testGetColumn()
    {
        $config = [
            'table'      => 'test_models',
            'columns'    => [
                'id' => 'integer',
            ],
            'values'     => [],
            'defaults'   => [],
            'timestamps' => false,
            'softDelete' => false,
            'uuid'       => false,
        ];

        $modelDefinition = new ModelDefinition('TestModel', $config);
        $this->assertCount(expectedCount: 1, haystack: $modelDefinition->columns);
        $this->assertInstanceOf(expected: ColumnDefinition::class, actual: $modelDefinition->getColumn('id'));
        $this->assertSame(expected: 'id', actual: $modelDefinition->getColumn('id')->name);
        $this->assertSame(expected: 'integer', actual: $modelDefinition->getColumn('id')->dataType);
        $this->assertSame(expected: 'int', actual: $modelDefinition->getColumn('id')->dataTypeCast);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::exceptions()
     */
    public function testExceptions(string $model, array $config, string $expectedException)
    {
        $this->expectException($expectedException);
        new ModelDefinition('TestModel', $config);
    }

    /**
     * @dataProvider \SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions\ModelDefinitionDataProvider::casts()
     */
    public function testGetCasts(string $model, array $config, array $expectedCasts): void
    {
        $modelDefinition = new ModelDefinition('TestModel', $config);
        $this->assertSame($expectedCasts, $modelDefinition->getCasts());
    }
}
