<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Tests\Unit\Models\Definitions;

use SimKlee\LaravelCraftingTable\Exceptions\MissingForeignKeyTypeDefinition;
use SimKlee\LaravelCraftingTable\Exceptions\MultipleDataTypeKeywordsFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\NoDataTypeKeywordFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\UnknownForeignKeyTypeDefinition;

class ModelDefinitionDataProvider
{
    public static function model(): array
    {
        return [
            'base'           => [
                'model'        => 'TestModel',
                'config'       => [
                    'table'      => 'test_models',
                    'columns'    => [],
                    'values'     => [],
                    'defaults'   => [],
                    'timestamps' => false,
                    'softDelete' => false,
                    'uuid'       => false,
                ],
                'expectations' => [
                    'columnCount'                => 0,
                    'hasDates'                   => false,
                    'getDatesCount'              => 0,
                    'columnsWithForeignKeyCount' => 0,
                ],
            ],
            'withTimestamps' => [
                'model'        => 'TestModel',
                'config'       => [
                    'table'      => 'test_models',
                    'columns'    => [],
                    'values'     => [],
                    'defaults'   => [],
                    'timestamps' => true,
                    'softDelete' => false,
                    'uuid'       => false,
                ],
                'expectations' => [
                    'columnCount'                => 2,
                    'hasDates'                   => true,
                    'getDatesCount'              => 2,
                    'columnsWithForeignKeyCount' => 0,
                ],
            ],
            'withSoftDelete' => [
                'model'        => 'TestModel',
                'config'       => [
                    'table'      => 'test_models',
                    'columns'    => [],
                    'values'     => [],
                    'defaults'   => [],
                    'timestamps' => false,
                    'softDelete' => true,
                    'uuid'       => false,
                ],
                'expectations' => [
                    'columnCount'                => 1,
                    'hasDates'                   => true,
                    'getDatesCount'              => 1,
                    'columnsWithForeignKeyCount' => 0,
                ],
            ],
            'withUuid'       => [
                'model'        => 'TestModel',
                'config'       => [
                    'table'      => 'test_models',
                    'columns'    => [],
                    'values'     => [],
                    'defaults'   => [],
                    'timestamps' => false,
                    'softDelete' => false,
                    'uuid'       => true,
                ],
                'expectations' => [
                    'columnCount'                => 1,
                    'hasDates'                   => false,
                    'getDatesCount'              => 0,
                    'columnsWithForeignKeyCount' => 0,
                ],
            ],
            'baseWithId'     => [
                'model'        => 'TestModel',
                'config'       => [
                    'table'      => 'test_models',
                    'columns'    => [
                        'id' => 'integer',
                    ],
                    'values'     => [],
                    'defaults'   => [],
                    'timestamps' => false,
                    'softDelete' => false,
                    'uuid'       => false,
                ],
                'expectations' => [
                    'columnCount'                => 1,
                    'hasDates'                   => false,
                    'getDatesCount'              => 0,
                    'columnsWithForeignKeyCount' => 0,
                ],
            ],
        ];
    }

    public static function exceptions(): array
    {
        return [
            'MissingForeignKeyTypeDefinition'        => [
                'model'             => 'TestModel',
                'config'            => [
                    'table'   => 'test_models',
                    'columns' => [
                        'test_column' => 'fk',
                    ],
                ],
                'expectedException' => MissingForeignKeyTypeDefinition::class,
            ],
            'UnknownForeignKeyTypeDefinition'        => [
                'model'             => 'TestModel',
                'config'            => [
                    'table'   => 'test_models',
                    'columns' => [
                        'test_column' => 'fk|type:unknown',
                    ],
                ],
                'expectedException' => UnknownForeignKeyTypeDefinition::class,
            ],
            'MultipleDataTypeKeywordsFoundException' => [
                'model'             => 'TestModel',
                'config'            => [
                    'table'   => 'test_models',
                    'columns' => [
                        'test_column' => 'string|integer',
                    ],
                ],
                'expectedException' => MultipleDataTypeKeywordsFoundException::class,
            ],
            'NoDataTypeKeywordFoundException'        => [
                'model'             => 'TestModel',
                'config'            => [
                    'table'   => 'test_models',
                    'columns' => [
                        'test_column' => 'nullable',
                    ],
                ],
                'expectedException' => NoDataTypeKeywordFoundException::class,
            ],
        ];
    }

    public static function casts(): array
    {
        return [
            'Base' => [
                'model'         => 'TestModel',
                'config'        => [
                    'table'   => 'test_models',
                    'columns' => [
                        'id'         => 'integer|unsigned|ai',
                        'some_int'   => 'tinyint',
                        'some_float' => 'decimal',
                        #'some_bool'  => 'boolean',
                    ],
                    'timestamps' => true,
                ],
                'expectedCasts' => [
                    'id'         => 'int',
                    'some_int'   => 'int',
                    'some_float' => 'float',
                ],
            ],
        ];
    }
}
