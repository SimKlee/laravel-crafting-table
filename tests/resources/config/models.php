<?php
return [
    'Author' => [
        'table'      => 'authors',
        'columns'    => [
            'id'          => 'integer|unsigned|ai',
            'first_name'  => 'string|length:50',
            'middle_name' => 'string|length:50|nullable',
            'last_name'   => 'string|length:50',
            'gender'      => 'string|length:6|nullable',
            'birthday'    => 'date|nullable',
        ],
        'values'     => [],
        'defaults'   => [
            'gender' => ['divers', 'female', 'male'],
        ],
        'timestamps' => true,
        'softDelete' => false,
        'uuid'       => true,
    ],
    'Book'   => [
        'table'      => 'books',
        'columns'    => [
            'id'                  => 'integer|unsigned|ai',
            'author_id'           => 'fk',
            'title'               => 'string|length:50',
            'genre'               => 'string|length:25|nullable',
            'format'              => 'string|length:10',
            'isbn'                => 'string|length:13',
            'publisher'           => 'string|length:80|nullable',
            'year_of_publication' => 'year|nullable',
            'edition'             => 'tinyint|unsigned|nullable',
            'price'               => 'decimal|precision:6|scale:2|unsigned|nullable',
        ],
        'values'     => [
            'format' => ['hard-cover', 'soft-cover', 'ebook'],
        ],
        'defaults'   => [],
        'timestamps' => true,
        'softDelete' => false,
        'uuid'       => true,
    ],
];