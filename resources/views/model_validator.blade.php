<?php /** @var \SimKlee\LaravelCraftingTable\Models\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

@foreach($generator->getUses() as $class)
use {{ $class }};
@endforeach

class {{ $modelDefinition->model }}Validator
{
    public static function rules(): array
    {
        return [
<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition $columnDefinition */ ?>
@foreach($modelDefinition->getFilteredColumns(['id', 'uuid', 'created_at', 'updated_at', 'deleted_at']) as $columnDefinition)
            {{ $modelDefinition->model }}::PROPERTY_{{ Str::upper($columnDefinition->name) }} => '{{ (new \SimKlee\LaravelCraftingTable\Models\Formatters\ValidationRuleFormatter($columnDefinition))->validationRule() }}',
@endforeach
        ];
    }
}
