<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition $relation */ ?>
    public function {{ $relation->name }}(): BelongsToMany
    {
        return $this->belongsToMany({{ $relation->model }}::class, {{ $relation->pivot }}::TABLE);
    }