<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition $relation */ ?>
    public function {{ $relation->name }}(): BelongsTo
    {
        return $this->belongsTo({{ $relation->model }}::class);
    }