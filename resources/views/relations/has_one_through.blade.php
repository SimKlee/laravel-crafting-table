<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition $relation */ ?>
    public function {{ $relation->name }}(): HasOneThrough
    {
        return $this->hasOneThrough({{ $relation->model }}::class, {{ $relation->pivot }}::class);
    }