<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition $relation */ ?>
    public function {{ $relation->name }}(): HasMany
    {
        return $this->hasMany({{ $relation->model }}::class);
    }