<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition $relation */ ?>
    public function {{ $relation->name }}(): HasOne
    {
        return $this->hasOne({{ $relation->model }}::class);
    }