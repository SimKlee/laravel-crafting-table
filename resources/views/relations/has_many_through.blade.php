<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition $relation */ ?>
    public function {{ $relation->name }}(): HasManyThrough
    {
        return $this->hasManyThrough({{ $relation->model }}::class, {{ $relation->pivot }}::class);
    }