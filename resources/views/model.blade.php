<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition $relationDefinition */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

@foreach($generator->getUses() as $class)
use {{ $class }};
@endforeach

/**
 * // Properties
@foreach($modelDefinition->columns as $columnDefinition)
 * {{ '@' }}property {{ $columnDefinition->dataTypeCast }} ${{ $columnDefinition->name }}
@endforeach
 *
 * // Relations
@foreach($modelDefinition->relations as $relationDefinition)
@if(in_array($relationDefinition->type, ['BelongsTo', 'HasOne', 'HasOneThrough']))
 * {{ '@' }}property {{ $relationDefinition->model }} ${{ $relationDefinition->name }}
@else
 * {{ '@' }}property Collection|{{ $relationDefinition->model }}[] ${{ $relationDefinition->name }}
@endif
@endforeach
 *
 * // Methods
 * {{ '@' }}method static {{ $modelDefinition->model }} find(int $id)
 * {{ '@' }}method static {{ $modelDefinition->model }} create(array $attributes)
 * {{ '@' }}method static {{ $modelDefinition->model }} firstOrCreate(array $attributes = [], array $values = [])
 * {{ '@' }}method static {{ $modelDefinition->model }}Query query()
 * {{ '@' }}method static {{ $modelDefinition->model }}Repository repository()
 * {{ '@' }}method static {{ $modelDefinition->model }} createFake(array $attributes = [])
 * {{ '@' }}method static {{ $modelDefinition->model }} makeFake(array $attributes = [])
 * {{ '@' }}method static Collection|{{ $modelDefinition->model }}[] createFakes(int $count, array $attributes = [])
 * {{ '@' }}method static Collection|{{ $modelDefinition->model }}[] makeFakes(int $count, array $attributes = [])
 */
class {{ $modelDefinition->model }} extends {{ $generator->getExtends() }}
{
@foreach($generator->getTraits() as $trait)
    use {{ $trait }};
@endforeach

    public const TABLE = '{{ $modelDefinition->table }}';

<?php /** @var \SimKlee\LaravelCraftingTable\Models\ColumnDefinition $columnDefinition */ ?>
@foreach($modelDefinition->columns as $columnDefinition)
    public const PROPERTY_{{ Str::upper($columnDefinition->name) }} = '{{ $columnDefinition->name }}';
@endforeach

@if(count($modelDefinition->values) > 0)
@foreach($modelDefinition->values as $column => $values)
@foreach($values as $value)
    public const {{ Str::upper($column) }}_{{ Str::upper(Str::replace('-', '_', $value)) }} = '{{ $value }}';
@endforeach

@endforeach
@endif
@if($modelDefinition->getColumnsWithForeignKey()->count() > 0)
@foreach($modelDefinition->getColumnsWithForeignKey() as $columnDefinition)
    public const WITH_{{ Str::upper(Str::snake($columnDefinition->foreignKeyModel)) }} = '{{ Str::lower(Str::snake($columnDefinition->foreignKeyModel)) }}';
@endforeach

@endif
    /** {{ '@' }}var string */
    protected $table = self::TABLE;

    /** {{ '@' }}var bool */
    public $timestamps = {{ $modelDefinition->timestamps ? 'true' : 'false' }};

    /** {{ '@' }}var array|string[] */
    protected $fillable = [];

    /** {{ '@' }}var array|string[] */
    protected $guarded = [];

    /** {{ '@' }}var array|string[] */
    protected $hidden = [];

@if($modelDefinition->hasDates())
    /** {{ '@' }}var array|string[] */
    protected $dates = [
<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition $column */ ?>
@foreach ($modelDefinition->getDates() as $columnDefinition)
        self::PROPERTY_{{ strtoupper($columnDefinition->name) }},
@endforeach
    ];
@else
    /** {{ '@' }}var array|string[] */
    protected $dates = [];
@endif

@if($modelDefinition->hasDates())
    /** {{ '@' }}var array|string[] */
    protected array $dateTypes = [
<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition $columnDefinition */ ?>
@foreach ($modelDefinition->getDates() as $columnDefinition)
        self::PROPERTY_{{ strtoupper($columnDefinition->name) }} => '{{ $columnDefinition->dataType }}',
@endforeach
    ];
@else
    /** {{ '@' }}var array|string[] */
    protected array $dateTypes = [];
@endif

    /** {{ '@' }}var array|string[] */
    protected $casts = [
@foreach($modelDefinition->getCasts() as $column => $castType)
        self::PROPERTY_{{ strtoupper($column) }} => '{{ $castType }}',
@endforeach
    ];

<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition $relationDefinition */ ?>
@foreach($modelDefinition->relations as $relationDefinition)
    {!! $relationDefinition->getFormatter()->toString() !!}

@endforeach
}
