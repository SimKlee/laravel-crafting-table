<?php /** @var \SimKlee\LaravelCraftingTable\Models\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

@foreach($generator->getUses() as $class)
use {{ $class }};
@endforeach

class {{ $modelDefinition->model }}{{ $generator->type }}Request extends {{ $generator->getExtends() }}
{
    protected array $booleanProperties = [
@foreach ($modelDefinition->getCasts('boolean') as $property => $cast)
        {{ $modelDefinition->model }}::PROPERTY_{{ Str::upper($property) }},
@endforeach
    ];

    public function rules() : array
    {
       return {{ $modelDefinition->model }}Validator::rules();
    }
}
