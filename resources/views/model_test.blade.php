<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

@foreach($generator->getUses() as $class)
use {{ $class }};
@endforeach

class {{ $modelDefinition->model }}Test extends {{ $generator->getExtends() }}
{
    protected function getModelClass(): string
    {
        return {{ $modelDefinition->model }}::class;
    }

    protected function getModelProperties(): Collection
    {
        return collect([
<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition $columnDefinition */ ?>
@foreach($modelDefinition->columns as $columnDefinition)
            {{ $modelDefinition->model }}::PROPERTY_{{ Str::upper($columnDefinition->name) }},
@endforeach
        ]);
    }
}
