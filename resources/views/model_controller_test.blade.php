<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition $columnDefinition */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

@foreach($generator->getUses() as $class)
use {{ $class }};
@endforeach

class {{ $modelDefinition->model }}ControllerTest extends {{ $generator->getExtends() }}
{
    public function testIndex(): void
    {
        $this->login();
        $response = $this->get(route('{{ $modelDefinition->table }}.index'));
        $response->assertStatus(200);
    }
    
    public function testCreate(): void
    {
        $this->login();
        $response = $this->get(route('{{ $modelDefinition->table }}.create'));
        $response->assertStatus(200);
    }
    
    public function testStore(): void
    {
        $this->login();
        ${{ $modelDefinition->getModelVarName() }} = {{ $modelDefinition->model }}::makeFake();
        $response = $this->post(route('{{ $modelDefinition->table }}.store'), ${{ $modelDefinition->getModelVarName() }}->toArray());
        $response->assertStatus(302);
        $response->assertRedirect(route('{{ $modelDefinition->table }}.index'));
        /** @var {{ $modelDefinition->model }} $stored */
        $stored = {{ $modelDefinition->model }}::query()->orderBy({{ $modelDefinition->model }}::PROPERTY_ID, 'desc')->limit(1)->first();
@foreach($modelDefinition->getFilteredColumns(['id', 'uuid', 'created_at', 'updated_at', 'deleted_at']) as $columnDefinition)
        $this->assertSame(
            expected: ${{ $modelDefinition->getModelVarName() }}->{{ $columnDefinition->name }},
            actual: $stored->{{ $columnDefinition->name }},
            message: '{{ $columnDefinition->name }}'
        );
@endforeach
    }
    
    public function testShow(): void
    {
        $this->login();
        ${{ $modelDefinition->getModelVarName() }} = {{ $modelDefinition->model }}::createFake();
        $response = $this->get(route('{{ $modelDefinition->table }}.show', ['{{ $modelDefinition->getModelVarName() }}' => ${{ $modelDefinition->getModelVarName() }}->uuid]));
        $response->assertStatus(200);
@foreach($modelDefinition->getFilteredColumns(['id', 'uuid']) as $columnDefinition)
        $response->assertSee(value: ${{ $modelDefinition->getModelVarName() }}->{{ $columnDefinition->name }});
@endforeach
    }
    
    public function testEdit(): void
    {
        $this->login();
        ${{ $modelDefinition->getModelVarName() }} = {{ $modelDefinition->model }}::createFake();
        $response = $this->get(route('{{ $modelDefinition->table }}.edit', ['{{ $modelDefinition->getModelVarName() }}' => ${{ $modelDefinition->getModelVarName() }}->{{ $modelDefinition->getModelPublicId() }}]));
        $response->assertStatus(200);
@foreach($modelDefinition->getFilteredColumns(['id', 'uuid', 'created_at', 'updated_at', 'deleted_at']) as $columnDefinition)
        $response->assertSee(value: ${{ $modelDefinition->getModelVarName() }}->{{ $columnDefinition->name }});
@endforeach
    }
    
    public function testUpdate(): void
    {
        $this->login();
        ${{ $modelDefinition->getModelVarName() }} = {{ $modelDefinition->model }}::createFake();
        $changes = {{ $modelDefinition->model }}::makeFake()->toArray();
        $response = $this->patch(route('{{ $modelDefinition->table }}.update', ['{{ $modelDefinition->getModelVarName() }}' => ${{ $modelDefinition->getModelVarName() }}->{{ $modelDefinition->getModelPublicId() }}]), $changes);
        $stored = {{ $modelDefinition->model }}::find(${{ $modelDefinition->getModelVarName() }}->id);
        $response->assertStatus(302);
        $response->assertRedirect(route('{{ $modelDefinition->table }}.index'));
        foreach ($changes as $property => $value) {
            if (in_array($property, ['id', 'uuid'])) {
                continue;
            }
            $storedValue = $stored->{$property};
            if ($storedValue instanceof Carbon) {
                $storedValue = $storedValue->toISOString();
            }
            $this->assertSame(expected: $value, actual: $storedValue, message: $property);
        }
    }
    
    public function testDestroy()
    {
        $this->login();
        ${{ $modelDefinition->getModelVarName() }} = {{ $modelDefinition->model }}::createFake();
        $response = $this->delete(route('{{ $modelDefinition->table }}.destroy', ['{{ $modelDefinition->getModelVarName() }}' => ${{ $modelDefinition->getModelVarName() }}->{{ $modelDefinition->getModelPublicId() }}]));
        $response->assertStatus(302);
        $response->assertRedirect(route('{{ $modelDefinition->table }}.index'));
        $this->assertNull({{ $modelDefinition->model }}::find(${{ $modelDefinition->getModelVarName() }}->id));
    }
}
