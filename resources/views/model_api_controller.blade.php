<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
declare(strict_types=1);

namespace App\Http\Controllers\Api;

@foreach($generator->getUses() as $class)
use {{ $class }};
@endforeach

class {{ $modelDefinition->model }}Controller extends Controller
{
    public function index(): AnonymousResourceCollection
    {
        return {{ $modelDefinition->model }}Resource::collection({{ $modelDefinition->model }}::all());
    }

    public function store({{ $modelDefinition->model }}StoreRequest $request): {{ $modelDefinition->model }}Resource
    {
        return new {{ $modelDefinition->model }}Resource({{ $modelDefinition->model }}::create($request->validated()));
    }

    public function show({{ $modelDefinition->model }} ${{ $modelDefinition->getModelVarName() }}): {{ $modelDefinition->model }}Resource
    {
        return new {{ $modelDefinition->model }}Resource(${{ $modelDefinition->getModelVarName() }});
    }

    public function update({{ $modelDefinition->model }}UpdateRequest $request, {{ $modelDefinition->model }} ${{ $modelDefinition->getModelVarName() }})
    {
        ${{ $modelDefinition->getModelVarName() }}->update($request->validated());

        return new {{ $modelDefinition->model }}Resource(${{ $modelDefinition->getModelVarName() }});
    }

    public function destroy({{ $modelDefinition->model }} ${{ $modelDefinition->getModelVarName() }}): Response
    {
        ${{ $modelDefinition->getModelVarName() }}->delete();

        return response();
    }
}
