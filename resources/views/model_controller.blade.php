<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

@foreach($generator->getUses() as $class)
use {{ $class }};
@endforeach

class {{ $modelDefinition->model }}Controller extends Controller
{
    public function index(Request $request): Factory|View
    {
        $query = {{ $modelDefinition->model }}::query();

        return view('{{ $modelDefinition->table }}.index')
            ->with('{{ $modelDefinition->table }}', $query->paginate()->withQueryString());
    }

    public function create(Request $request): Factory|View
    {
        return view('{{ $modelDefinition->table }}.create');
    }

    public function store({{ $modelDefinition->model }}StoreRequest $request, string $redirectTo = '{{ $modelDefinition->table }}.index'): RedirectResponse
    {
        {{ $modelDefinition->model }}::create($request->validated());

        if ($request->has('redirect')) {
            return redirect($request->get('redirect'));
        }

        return redirect()->route($redirectTo);
    }

    public function show(Request $request, {{ $modelDefinition->model }} ${{ $modelDefinition->getModelVarName() }}): Factory|View
    {
        return view('{{ $modelDefinition->table }}.show')
            ->with('{{ $modelDefinition->getModelVarName() }}', ${{ $modelDefinition->getModelVarName() }});
    }

    public function edit(Request $request, {{ $modelDefinition->model }} ${{ $modelDefinition->getModelVarName() }}): Factory|View
    {
        return view('{{ $modelDefinition->table }}.edit')
            ->with('{{ $modelDefinition->getModelVarName() }}', ${{ $modelDefinition->getModelVarName() }});
    }

    public function update({{ $modelDefinition->model }}UpdateRequest $request, {{ $modelDefinition->model }} ${{ $modelDefinition->getModelVarName() }}, string $redirectTo = '{{ $modelDefinition->table }}.index'): RedirectResponse
    {
        ${{ $modelDefinition->getModelVarName() }}->update($request->validated());

        return redirect()->route($redirectTo);
    }

    public function destroy(Request $request, {{ $modelDefinition->model }} ${{ $modelDefinition->getModelVarName() }}, string $redirectTo = '{{ $modelDefinition->table }}.index'): RedirectResponse
    {
        ${{ $modelDefinition->getModelVarName() }}->delete();

        return redirect()->route($redirectTo);
    }
}
