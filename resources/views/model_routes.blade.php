<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

@foreach($generator->getUses() as $class)
use {{ $class }};
@endforeach

class {{ $modelDefinition->model }}Routes extends {{ $generator->getExtends() }}
{
    public static function apiRoutes(): void
    {
        Route::resource(
            name:       '{{ $modelDefinition->table }}',
            controller: {{ $modelDefinition->model }}ApiController::class
        );
    }

    public static function webRoutes(): void
    {
        Route::resource(
            name:       '{{ $modelDefinition->table }}',
            controller: {{ $modelDefinition->model }}Controller::class
        );
    }
}
