<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

@foreach($generator->getUses() as $class)
use {{ $class }};
@endforeach

class {{ $modelDefinition->model }}Repository extends {{ $generator->getExtends() }}
{
    public function lookup(): array
    {
        return {{ $modelDefinition->model }}::query()
                ->orderBy({{ $modelDefinition->model }}::PROPERTY_{{ strtoupper($modelDefinition->lookupLabel()) }})
                ->get()
                ->mapWithKeys(function ({{ $modelDefinition->model }} ${{ $modelDefinition->getModelVarName() }}) {
                    return [${{ $modelDefinition->getModelVarName() }}->id => ${{ $modelDefinition->getModelVarName() }}->{{ $modelDefinition->lookupLabel() }}];
                })->toArray();
    }
}
