<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

use App\Models\{{ $modelDefinition->model }};
use Illuminate\Database\Eloquent\Factories\Factory;

class {{ $modelDefinition->model }}Factory extends Factory
{
    public function definition(): array
    {
        return [
<?php /** @var \SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition $columnDefinition */ ?>
@foreach($modelDefinition->getFilteredColumns(['id', 'created_at', 'updated_at', 'deleted_at']) as $columnDefinition)
            {{ $modelDefinition->model }}::PROPERTY_{{ Str::upper($columnDefinition->name) }} => {!! \SimKlee\LaravelCraftingTable\Generators\Formatters\ColumnFakerFormatter::create($modelDefinition->model, $columnDefinition)->toString() !!},
@endforeach
        ];
    }
}
