<?php /** @var \SimKlee\LaravelCraftingTable\Models\ModelDefinition $modelDefinition */ ?>
<?php /** @var \SimKlee\LaravelCraftingTable\Generators\AbstractGenerator $generator */ ?>
declare(strict_types=1);

namespace {{ $generator::namespace() }};

use Illuminate\Support\Collection;
use App\Models\{{ $modelDefinition->model }};
@foreach($generator->getUses() as $class)
 use {{ $class }};
@endforeach

/**
* {{ '@' }}method {{ $modelDefinition->model }} first()
* {{ '@' }}method Collection|{{ $modelDefinition->model }}[] get(array $columns = ['*'])
*/
class {{ $modelDefinition->model }}Query extends {{ $generator->getExtends() }}
{

}
