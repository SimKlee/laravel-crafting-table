<?php

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Controller;
use SimKlee\LaravelCraftingTable\Http\Requests\AbstractFormRequest;
use SimKlee\LaravelCraftingTable\Models\AbstractModel;
use SimKlee\LaravelCraftingTable\Models\AbstractModelQuery;
use SimKlee\LaravelCraftingTable\Models\AbstractModelRepository;
use SimKlee\LaravelCraftingTable\Models\AbstractModelRoutes;
use SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition;
use SimKlee\LaravelCraftingTable\Tests\ControllerTestCase;
use SimKlee\LaravelCraftingTable\Tests\ModelTestCase;

return [
    'model'               => [
        'extends'   => AbstractModel::class,
        'namespace' => 'App\Models',
    ],
    'model_test'          => [
        'extends'   => ModelTestCase::class,
        'namespace' => 'Tests\Feature\Models',
    ],
    'web_controller'      => [
        'extends'   => Controller::class,
        'namespace' => 'App\Http\Controllers',
    ],
    'web_controller_test' => [
        'extends'   => ControllerTestCase::class,
        'namespace' => 'Tests\Feature\Http\Controllers',
    ],
    'api_controller'      => [
        'extends'   => Controller::class,
        'namespace' => 'App\Http\Controllers\Api',
    ],
    'resource'      => [
        'extends'   => JsonResource::class,
        'namespace' => 'App\Http\Resources',
    ],
    'query'               => [
        'extends'   => AbstractModelQuery::class,
        'namespace' => 'App\Database\Queries',
    ],
    'route'               => [
        'extends'   => AbstractModelRoutes::class,
        'namespace' => 'App\Http\Routes',
    ],
    'request'             => [
        'extends'   => AbstractFormRequest::class,
        'namespace' => 'App\Http\Requests',
    ],
    'validator'           => [
        'extends'   => null,
        'namespace' => 'App\Http\Requests\Validators',
    ],
    'repository'          => [
        'extends'   => AbstractModelRepository::class,
        'namespace' => 'App\Repositories',
    ],
    'factory'             => [
        'extends'   => Factory::class,
        'namespace' => 'Database\Factories',
    ],
    'seeder'              => [
        'extends'   => Seeder::class,
        'namespace' => 'Database\Seeders',
    ],
    'view_relations' => [
        'generate' => true,
        'relations' => [
            RelationDefinition::TYPE_HAS_MANY,
        ],
    ],
];