<?php
/** @see https://github.com/SimKlee/laravel-crafting-table for full documentation of the model definitions */
return [
    'Model' => [
        'namespace'  => null,
        'table'      => null,
        'columns'    => [],
        'values'     => [],
        'defaults'   => [],
        'timestamps' => false,
        'softDelete' => false,
        'uuid'       => false,
        'factory'    => [],
        'samples'    => 0,
        'views'      => [
            'index'  => [],
            'edit'   => [],
            'create' => [],
            'show'   => [],
        ],
        'lookup'     => [
            'label' => null,
        ],
        'relations'  => [
            'MethodName' => [
                'type'  => 'HasOne|BelongsTo|HasMany|BelongsToMany',
                'model' => 'TargetModel',
                'pivot' => 'PivotModel', // only for BelongsToMany
            ],
        ],
    ],
];
