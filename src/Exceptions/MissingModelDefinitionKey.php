<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Exceptions;

use Exception;

class MissingModelDefinitionKey extends Exception
{

}
