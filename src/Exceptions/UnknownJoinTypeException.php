<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Exceptions;

class UnknownJoinTypeException extends \Exception
{
}
