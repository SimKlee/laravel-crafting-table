<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Console\Commands;

abstract class AbstractCrafterCommand extends AbstractCommand
{
    abstract public function handle(): int;

    protected function askForModel(): string
    {
        return $this->choice('Model', array_keys(config('models')));
    }
}
