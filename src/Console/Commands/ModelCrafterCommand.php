<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Console\Commands;

use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SimKlee\LaravelCraftingTable\Exceptions\MissingForeignKeyTypeDefinition;
use SimKlee\LaravelCraftingTable\Exceptions\MissingValidatorException;
use SimKlee\LaravelCraftingTable\Exceptions\MultipleDataTypeKeywordsFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\NoCastTypeForDataTypeException;
use SimKlee\LaravelCraftingTable\Exceptions\NoDataTypeKeywordFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\UnknownForeignKeyTypeDefinition;
use SimKlee\LaravelCraftingTable\Generators\ConfigGenerator;
use SimKlee\LaravelCraftingTable\Generators\ForeignListViewGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelApiControllerGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelControllerTestGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelResourceGenerator;
use SimKlee\LaravelCraftingTable\Generators\ViewGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelFactoryGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelMigrationGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelQueryGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelRepositoryGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelRequestGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelRoutesGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelTestGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelValidatorGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelWebControllerGenerator;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinitionBag;
use SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition;
use SimKlee\LaravelCraftingTable\Support\SystemCommand;
use Symfony\Component\Finder\SplFileInfo;

class ModelCrafterCommand extends AbstractCrafterCommand
{
    private const ARGUMENT_NAME     = 'name';
    private const OPTION_ALL        = 'all';
    private const OPTION_FULL       = 'full';
    private const OPTION_MIGRATION  = 'migration';
    private const OPTION_REPOSITORY = 'repository';
    private const OPTION_QUERY      = 'query';
    private const OPTION_BEAUTIFY   = 'beautify';
    private const OPTION_TESTS      = 'tests';
    private const OPTION_MIGRATE    = 'migrate';
    private const OPTION_FRESH      = 'fresh';
    private const OPTION_STATUS     = 'status';
    private const OPTION_SEED       = 'seed';
    private const OPTION_DELETE     = 'delete';

    /** @var string */
    protected $signature = 'craft:model {name? : Name of the model}
                                        {--a|all : Run for all Models in config file}
                                        {--full : Create all classes (-MRQT)}
                                        {--M|migration : Create Migration}
                                        {--R|repository : Create Repository}
                                        {--Q|query : Create Query}
                                        {--T|tests : Create Test classes for all generated classes}
                                        {--beautify : Beautify all generated files}
                                        {--migrate : Runs the migration after generating files}
                                        {--fresh : Runs a fresh migration after generating files}
                                        {--seed : Run migration with --seed option }
                                        {--status : Show status after migration}
                                        {--delete : Delete old generation}';

    /** @var string */
    protected $description = '';

    private ModelDefinitionBag $bag;

    private string $beautifierBin = './vendor/bin/phpcbf';

    private Collection $writtenFiles;

    private int $migrationCount = 0;

    /**
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws MissingForeignKeyTypeDefinition
     * @throws MissingValidatorException
     * @throws UnknownForeignKeyTypeDefinition
     */
    public function __construct()
    {
        parent::__construct();

        $this->bag          = new ModelDefinitionBag(config('models'));
        $this->writtenFiles = new Collection();
    }

    public function handle(): int
    {
        $this->start = microtime(as_float: true);
        $ukraine     = "\e[1;37;43m#StandWith\e[0m\e[1;37;44mUkraine\e[0m";
        $this->info('Laravel Crafting Table ' . $ukraine);

        if ($this->option(self::OPTION_ALL)) {
            $this->migrationCount = 1;
            collect(array_keys(config('models')))->each(function (string $model) {
                $this->processModel($model);
            });
        } else {
            $this->processModel($this->argument(self::ARGUMENT_NAME) ?? $this->askForModel());
        }

        if ($this->option(self::OPTION_DELETE)) {
            $this->call('migrate:fresh', ['--seed' => true]);
        }

        $this->printSummary();

        return self::SUCCESS;
    }

    private function processModel(string $model): void
    {
        if ($this->option(self::OPTION_DELETE)) {
            $this->components->task('Deleting models', function () use ($model) {
                $this->callSilent('craft:delete', ['name' => $model]);
            });

            return;
        }

        $this->newLine();
        $this->components->info('Crafting ' . $model);
        $this->components->task('Generating files', function () use ($model) {

            $this->newLine();

            $modelDefinition = $this->bag->get($model);
            $deleted         = $this->deleteOldMigrations($model);

            $this->writeModel($modelDefinition);
            $this->writeModelMigration($modelDefinition);
            $this->writeModelQuery($modelDefinition);
            $this->writeModelRepository($modelDefinition);
            $this->writeModelValidator($modelDefinition);

            $this->writeModelStoreRequest($modelDefinition);
            $this->writeModelUpdateRequest($modelDefinition);

            $this->writeModelWebController($modelDefinition);
            $this->writeModelApiController($modelDefinition);
            $this->writeModelResource($modelDefinition);
            $this->writeModelRoutes($modelDefinition);

            $this->writeIndexView($modelDefinition);
            $this->writeEditView($modelDefinition);
            $this->writeCreateView($modelDefinition);
            $this->writeShowView($modelDefinition);
            $this->writeDeleteView($modelDefinition);
            if (config('craftingtable.view_relations.generate', true)) {
                $this->writeForeignListViews($modelDefinition);
            }

            $this->writeLangFile($modelDefinition);
            $this->migrate($deleted);
            $this->writeModelFactory($modelDefinition);

            $this->writeModelTest($modelDefinition);
            $this->writeModelWebControllerTest($modelDefinition);
        });
    }

    private function migrate(int $deleted): void
    {
        if (($this->option(self::OPTION_MIGRATION) || $this->option(self::OPTION_FULL)) && $this->option(self::OPTION_MIGRATE)) {
            $this->info('');
            $this->info('Migrating ...');
            $arguments = [];
            if ($this->option(self::OPTION_SEED)) {
                $arguments['--seed'] = true;
            }
            $command = $this->option(self::OPTION_FRESH) ? 'migrate:fresh' : 'migrate';
            if ($deleted > 0 && !$this->option(self::OPTION_FRESH)) {
                $this->warn('After deleting an existing model the migration must be refreshed.');
                $command = 'migrate:fresh';
            }
            $this->call($command, $arguments);
            if ($this->option(self::OPTION_STATUS)) {
                $this->info('');
                $this->info('Migration Status:');
                $this->call('migrate:status');
            }
        }
    }

    private function runCodeBeautifier(string $file): void
    {
        if ($this->option(self::OPTION_BEAUTIFY)) {
            SystemCommand::create(sprintf('%s %s', $this->beautifierBin, $file))->run(quiet: true);
            $this->info(sprintf('Beautified %s', $file));
        }
    }

    private function writeModel(ModelDefinition $modelDefinition): void
    {
        $generator = new ModelGenerator($modelDefinition);
        $file      = sprintf('%s/%s.php', $generator->getClassPath(), $modelDefinition->model);
        $generator->write(file: $file, override: true);
        $this->printWrittenFile('Model', $file);
        $this->runCodeBeautifier($file);
        $this->writtenFiles->add($file);
    }

    private function writeModelQuery(ModelDefinition $modelDefinition): void
    {
        if ($this->option(self::OPTION_QUERY) || $this->option(self::OPTION_FULL)) {
            $generator = new ModelQueryGenerator($modelDefinition);
            $file      = sprintf('%s/%sQuery.php', $generator->getClassPath(), $modelDefinition->model);
            $generator->write(file: $file, override: true);
            $this->printWrittenFile('Query', $file);
            $this->runCodeBeautifier($file);
            $this->writtenFiles->add($file);
        }
    }

    private function writeModelRepository(ModelDefinition $modelDefinition): void
    {
        if ($this->option(self::OPTION_REPOSITORY) || $this->option(self::OPTION_FULL)) {
            $generator = new ModelRepositoryGenerator($modelDefinition);
            $file      = sprintf('%s/%sRepository.php', $generator->getClassPath(), $modelDefinition->model);
            $generator->write(file: $file, override: true);
            $this->printWrittenFile('Repository', $file);
            $this->runCodeBeautifier($file);
            $this->writtenFiles->add($file);
        }
    }

    private function writeModelFactory(ModelDefinition $modelDefinition): void
    {
        $generator = new ModelFactoryGenerator($modelDefinition);
        $file      = sprintf('database/factories/%sFactory.php', $modelDefinition->model);
        $generator->write(file: $file, override: true);
        $this->printWrittenFile('Factory', $file);
        $this->runCodeBeautifier($file);

        if (config(sprintf('models.%s.samples', $modelDefinition->model)) > 0) {
            $factoryClass = sprintf('%s\%s', ModelGenerator::namespace(), $modelDefinition->model);
            call_user_func([$factoryClass, 'createFakes'], config(sprintf('models.%s.samples', $modelDefinition->model)));
        }

        $this->writtenFiles->add($file);
    }

    private function writeModelRoutes(ModelDefinition $modelDefinition): void
    {
        $generator = new ModelRoutesGenerator($modelDefinition);
        $file      = sprintf('%s/%sRoutes.php', $generator->getClassPath(), $modelDefinition->model);
        $generator->write(file: $file, override: true);
        $this->printWrittenFile('Routes', $file);
        $this->runCodeBeautifier($file);
        $this->writtenFiles->add($file);
    }

    private function writeModelWebController(ModelDefinition $modelDefinition): void
    {
        $generator = new ModelWebControllerGenerator($modelDefinition);
        $file      = sprintf('%s/%sController.php', $generator->getClassPath(), $modelDefinition->model);
        $generator->write(file: $file, override: true);
        $this->printWrittenFile('Controller', $file);
        $this->runCodeBeautifier($file);
        $this->writtenFiles->add($file);
    }

    private function writeModelApiController(ModelDefinition $modelDefinition): void
    {
        $generator = new ModelApiControllerGenerator($modelDefinition);
        $file      = sprintf('%s/%sController.php', $generator->getClassPath(), $modelDefinition->model);
        $generator->write(file: $file, override: true);
        $this->printWrittenFile('Controller', $file);
        $this->runCodeBeautifier($file);
        $this->writtenFiles->add($file);
    }

    private function writeModelResource(ModelDefinition $modelDefinition): void
    {
        $generator = new ModelResourceGenerator($modelDefinition);
        $file      = sprintf('%s/%sResource.php', $generator->getClassPath(), $modelDefinition->model);
        $generator->write(file: $file, override: true);
        $this->printWrittenFile('Resource', $file);
        $this->runCodeBeautifier($file);
        $this->writtenFiles->add($file);
    }

    private function writeModelStoreRequest(ModelDefinition $modelDefinition): void
    {
        $generator = new ModelRequestGenerator($modelDefinition, ModelRequestGenerator::TYPE_STORE);
        $file      = sprintf('%s/%s%sRequest.php', $generator->getClassPath(), $modelDefinition->model, ModelRequestGenerator::TYPE_STORE);
        $generator->write(file: $file, override: true);
        $this->printWrittenFile('Request', $file);
        $this->runCodeBeautifier($file);
        $this->writtenFiles->add($file);
    }

    private function writeModelUpdateRequest(ModelDefinition $modelDefinition): void
    {
        $generator = new ModelRequestGenerator($modelDefinition, ModelRequestGenerator::TYPE_UPDATE);
        $file      = sprintf('%s/%s%sRequest.php', $generator->getClassPath(), $modelDefinition->model, ModelRequestGenerator::TYPE_UPDATE);
        $generator->write(file: $file, override: true);
        $this->printWrittenFile('Request', $file);
        $this->runCodeBeautifier($file);
        $this->writtenFiles->add($file);
    }

    private function writeModelValidator(ModelDefinition $modelDefinition): void
    {
        $generator = new ModelValidatorGenerator($modelDefinition);
        $file      = sprintf('%s/%sValidator.php', $generator->getClassPath(), $modelDefinition->model);
        $generator->write(file: $file, override: true);
        $this->printWrittenFile('Validator', $file);
        $this->runCodeBeautifier($file);
        $this->writtenFiles->add($file);
    }

    private function writeModelTest(ModelDefinition $modelDefinition): void
    {
        if ($this->option(self::OPTION_TESTS) || $this->option(self::OPTION_FULL)) {
            $generator = new ModelTestGenerator($modelDefinition);
            $file      = sprintf('%s/%sTest.php', $generator->getClassPath(), $modelDefinition->model);
            $generator->write(file: $file, override: true);
            $this->printWrittenFile('Test', $file);
            $this->runCodeBeautifier($file);
            $this->writtenFiles->add($file);
        }
    }

    private function writeModelWebControllerTest(ModelDefinition $modelDefinition): void
    {
        if ($this->option(self::OPTION_TESTS) || $this->option(self::OPTION_FULL)) {
            $generator = new ModelControllerTestGenerator($modelDefinition);
            $file      = sprintf('%s/%sControllerTest.php', $generator->getClassPath(), $modelDefinition->model);
            $generator->write(file: $file, override: true);
            $this->printWrittenFile('Test', $file);
            $this->runCodeBeautifier($file);
            $this->writtenFiles->add($file);
        }
    }

    private function writeModelMigration(ModelDefinition $modelDefinition): void
    {
        if ($this->option(self::OPTION_MIGRATION) || $this->option(self::OPTION_FULL)) {
            $generator = new ModelMigrationGenerator($modelDefinition);
            $now       = Carbon::now();
            if ($this->migrationCount > 0) {
                $now->addSeconds($this->migrationCount);
                $this->migrationCount++;
            }
            $datetime = $now->format('Y_m_d_His');
            $name     = sprintf('create_%s_table', Str::lower(Str::snake($modelDefinition->model)));
            $file     = sprintf('database/migrations/%s_%s.php', $datetime, $name);
            $generator->write(file: $file, override: true);
            $this->printWrittenFile('Migration', $file);
            $this->runCodeBeautifier($file);
            $this->writtenFiles->add($file);
        }
    }

    private function writeIndexView(ModelDefinition $modelDefinition): void
    {
        $blade = new ViewGenerator($this->bag, $modelDefinition, 'index');
        $file  = $blade->write();

        $this->printWrittenFile('View', $file);
        $this->writtenFiles->add($file);
    }

    private function writeEditView(ModelDefinition $modelDefinition): void
    {
        $blade = new ViewGenerator($this->bag, $modelDefinition, 'edit');
        $file  = $blade->write();

        $this->printWrittenFile('View', $file);
        $this->writtenFiles->add($file);
    }

    private function writeCreateView(ModelDefinition $modelDefinition): void
    {
        $blade = new ViewGenerator($this->bag, $modelDefinition, 'create');
        $file  = $blade->write();

        $this->printWrittenFile('View', $file);
        $this->writtenFiles->add($file);
    }

    private function writeShowView(ModelDefinition $modelDefinition): void
    {
        $blade = new ViewGenerator($this->bag, $modelDefinition, 'show');
        $file  = $blade->write();

        $this->printWrittenFile('View', $file);
        $this->writtenFiles->add($file);
    }

    private function writeDeleteView(ModelDefinition $modelDefinition): void
    {
        $blade = new ViewGenerator($this->bag, $modelDefinition, 'delete_modal');
        $file  = $blade->write();
        $this->printWrittenFile('View', $file);
        $this->writtenFiles->add($file);
    }

    private function writeForeignListViews(ModelDefinition $modelDefinition): void
    {
        $modelDefinition->relations
            ->filter(function (RelationDefinition $relationDefinition) {
                return in_array($relationDefinition->type, config('craftingtable.view_relations.relations', [RelationDefinition::TYPE_HAS_MANY]));
            })->each(function (RelationDefinition $relationDefinition) use ($modelDefinition) {
                $blade = new ForeignListViewGenerator($this->bag, $modelDefinition, $relationDefinition);
                $file  = $blade->write();
                $this->printWrittenFile('View', $file);
                $this->writtenFiles->add($file);
            });
    }

    private function writeLangFile(ModelDefinition $modelDefinition): void
    {
        $config = new ConfigGenerator();
        $config->setValue('titles', [
            'index'  => ucwords(Str::replace('_', ' ', $modelDefinition->table)),
            'show'   => 'Show ' . ucwords(Str::replace('_', ' ', Str::singular($modelDefinition->table))),
            'create' => 'Create ' . ucwords(Str::replace('_', ' ', Str::singular($modelDefinition->table))),
            'edit'   => 'Edit ' . ucwords(Str::replace('_', ' ', Str::singular($modelDefinition->table))),
            'delete' => 'Delete ' . ucwords(Str::replace('_', ' ', Str::singular($modelDefinition->table))),
        ]);
        $config->setValue('properties', $modelDefinition->columns->mapWithKeys(function (ColumnDefinition $columnDefinition) {
            return [$columnDefinition->name => ucwords(Str::replace('_', ' ', $columnDefinition->name))];
        })->toArray());
        $file = lang_path(sprintf('en/%s.php', $modelDefinition->table));
        $config->write($file);

        $this->printWrittenFile('Language', $file);
        $this->writtenFiles->add($file);
    }

    private function deleteOldMigrations(string $model): int
    {
        if ($this->option(self::OPTION_MIGRATION) || $this->option(self::OPTION_FULL)) {
            $oldMigrations = $this->findOlderMigrations($model);
            if ($oldMigrations->count() > 0) {
                $this->components->warn('Deleting old migrations for model ' . $model . ':');
                $this->components->bulletList(
                    $oldMigrations->map(function (\SplFileInfo $splFileInfo) {
                        return $splFileInfo->getBasename();
                    })->toArray()
                );
                $oldMigrations->each(function (\SplFileInfo $splFileInfo) {
                    File::delete($splFileInfo->getRealPath());
                });
            }

            return $oldMigrations->count();
        }

        return 0;
    }

    private function findOlderMigrations(string $model): Collection
    {
        return collect(File::files(database_path('migrations')))
            ->filter(function (SplFileInfo $splFileInfo) use ($model) {
                return $model === $this->getModelNameFromMigrationFileName($splFileInfo->getFilename());
            });
    }

    private function getModelNameFromMigrationFileName(string $fileName): string
    {
        $parts = explode(separator: '_', string: $fileName);
        if (count($parts) < 7) {
            $this->error('Corrupt filename: ' . $fileName);
        }

        $parts = array_slice($parts, 5);
        array_pop($parts);

        return Str::ucfirst(Str::camel(implode(separator: '_', array: $parts)));
    }

    private function printSummary(): void
    {
        $this->newLine();
        $this->line('  <bg=bright-yellow> SUMMARY </>');
        $this->newLine();
        $this->components->twoColumnDetail('Written files', count($this->writtenFiles));
        $loc  = $this->writtenFiles
            ->map(function (string $file) {
                return File::lines($file)->count();
            })->sum();
        $size = $this->writtenFiles
            ->map(function (string $file) {
                return filesize($file);
            })->sum();
        $this->components->twoColumnDetail('Lines of code', number_format($loc, 0));
        $this->components->twoColumnDetail('Size (in bytes)', number_format($size, 0));
        $duration = microtime(as_float: true) - $this->start;
        $this->components->twoColumnDetail('Duration (in seconds)', number_format($duration, 4));
    }

    private function printLOC(): void
    {

    }

    private function showCraftingTable(): void
    {
        $this->info('+----------------------------------------------+');
        $this->info('| --- Welcome to Laravel Crafting Table -------+');
        $this->info('+----------------------------------------------+');
        $this->info('');
        $this->info("    MMMMMMMMMMMMMMN0o:;;,,cxKWMMMMMMMMMMMMMM");
        $this->info("    MMMMMMMMMMMNOo:;'.;::,.,;cd0WMMMMMMMMMMM");
        $this->info("    MMMMMMMWXkl::oOXXOl:cd0NXkl::o0NMMMMMMMM");
        $this->info("    MMMMWXkl::o0NWXkc,,;,';o0NWXkl::oONMMMMM");
        $this->info("    MWKxc'.l0WWXkc'',..'..',.,lONWNO;.,lONMM");
        $this->info("    No..c;.xWWx'.,'....,.....;,.;0MNc.c;.'kW");
        $this->info("    X;.,;..xWWKdc,..,.....''..;lkXMNc.':'.oW");
        $this->info("    Nc'kKxc:cxKWWKxc,..,'..;lkXWNOo::lkXo.xM");
        $this->info("    Wd.xMMWXo..:d0WWXxc::oONWNOl;.,kNMMNc'OM");
        $this->info("    Mk'lWMMMK,'ol::llccccclol::ol.lNMMMX;,KM");
        $this->info("    M0':NMMMX;,KMNOl,.';,..;d0NMk.oWMMM0':NM");
        $this->info("    MX;,KMMMN:,0MMMMNO,..cKWMMMMx.dWMMMk.oWM");
        $this->info("    MNc'OMMMNc'OMMMMMWl';kMMMMMWo.xMMMWo.xMM");
        $this->info("    MWx';kXWWo'kMMMMMWl';kMMMMMWl'kMWKd,;0MM");
        $this->info("    MMW0o:;lkl.xMMMMMWl';kMMMMMNc'dxc;cxXWMM");
        $this->info("    MMMMMNOo;..lNMMMMWl';kMMMMMK;.':dKWMMMMM");
        $this->info("    MMMMMMMMNOl;:dKWMWl';kMMNOl;;o0WMMMMMMMM");
        $this->info("    MMMMMMMMMMWXkc;cx0l';x0o:;lONMMMMMMMMMMM");
        $this->info("    MMMMMMMMMMMMMWKxc;. .';lkNMMMMMMMMMMMMMM");
        $this->info("    MMMMMMMMMMMMMMMMNk;.'c0WMMMMMMMMMMMMMMMM");
        $this->info('');
    }

    private function printWrittenFile(string $type, string $file)
    {
        $this->components->twoColumnDetail(strtoupper($type), $file);
    }
}
