<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Console\Commands;

use Illuminate\Console\Command;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinitionBag;

class BladeGenerator extends Command
{
    /** @var string */
    protected $signature = 'craft:blade';

    /** @var string */
    protected $description = '';

    public function handle(): int
    {
        $bag             = new ModelDefinitionBag(config('models'));
        $modelDefinition = $bag->get('Platform');

        $blade = new \SimKlee\LaravelCraftingTable\Generators\ViewGenerator($modelDefinition, 'index');
        $blade->write();

        return self::SUCCESS;
    }

}
