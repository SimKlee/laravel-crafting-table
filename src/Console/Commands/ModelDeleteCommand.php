<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Console\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SimKlee\LaravelCraftingTable\Exceptions\MissingForeignKeyTypeDefinition;
use SimKlee\LaravelCraftingTable\Exceptions\MissingValidatorException;
use SimKlee\LaravelCraftingTable\Exceptions\MultipleDataTypeKeywordsFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\NoCastTypeForDataTypeException;
use SimKlee\LaravelCraftingTable\Exceptions\NoDataTypeKeywordFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\UnknownForeignKeyTypeDefinition;
use SimKlee\LaravelCraftingTable\Generators\ModelApiControllerGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelControllerTestGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelQueryGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelRepositoryGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelRequestGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelResourceGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelRoutesGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelTestGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelValidatorGenerator;
use SimKlee\LaravelCraftingTable\Generators\ModelWebControllerGenerator;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinitionBag;
use Symfony\Component\Finder\SplFileInfo;

class ModelDeleteCommand extends AbstractCrafterCommand
{
    private const ARGUMENT_NAME = 'name';

    /** @var string */
    protected $signature = 'craft:delete {name? : Name of the model} {--all}';

    /** @var string */
    protected $description = '';

    private ModelDefinitionBag $bag;

    /**
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws MissingForeignKeyTypeDefinition
     * @throws UnknownForeignKeyTypeDefinition
     * @throws MissingValidatorException
     */
    public function __construct()
    {
        parent::__construct();

        $this->bag = new ModelDefinitionBag(config('models'));
    }

    public function handle(): int
    {
        if ($this->option('all')) {
            $this->bag->getModelDefinitions()->each(function (ModelDefinition $modelDefinition) {
                $this->handleModel($modelDefinition->model);
            });

            return self::SUCCESS;
        }

        $this->handleModel($this->argument(self::ARGUMENT_NAME));

        return self::SUCCESS;
    }

    private function handleModel(string $model): void
    {
        $modelDefinition = $this->bag->get($model);

        $files = new Collection([
            sprintf('%s/%s.php', ModelGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sTest.php', ModelTestGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sQuery.php', ModelQueryGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sRepository.php', ModelRepositoryGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sRoutes.php', ModelRoutesGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sController.php', ModelWebControllerGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sController.php', ModelApiControllerGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sControllerTest.php', ModelControllerTestGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sValidator.php', ModelValidatorGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sResource.php', ModelResourceGenerator::path(), $modelDefinition->model),
            #sprintf('%s/%sController.php', ModelApiControllerGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sStoreRequest.php', ModelRequestGenerator::path(), $modelDefinition->model),
            sprintf('%s/%sUpdateRequest.php', ModelRequestGenerator::path(), $modelDefinition->model),
            database_path(sprintf('factories/%sFactory.php', $modelDefinition->model)),
            resource_path(sprintf('views/%s/index.blade.php', $modelDefinition->table)),
            resource_path(sprintf('views/%s/edit.blade.php', $modelDefinition->table)),
            resource_path(sprintf('views/%s/create.blade.php', $modelDefinition->table)),
            resource_path(sprintf('views/%s/show.blade.php', $modelDefinition->table)),
            resource_path(sprintf('views/%s/delete_modal.blade.php', $modelDefinition->table)),
            lang_path(sprintf('en/%s.php', $modelDefinition->table)),
        ]);

        collect(File::files(database_path('migrations')))
            ->each(function (SplFileInfo $splFileInfo) use ($modelDefinition, $files) {
                if (Str::contains(haystack: $splFileInfo->getBasename(), needles: sprintf('create_%s_table', Str::singular($modelDefinition->table)))) {
                    $files->add($splFileInfo->getRealPath());
                }
            });

        $files->each(function (string $file) {
            if (File::exists($file)) {
                File::delete($file);
                $this->info('Deleted ' . $file);
            } else {
                $this->warn('Not found: ' . $file);
            }
        });

        $viewPath = resource_path('views/' . $modelDefinition->table);
        if (File::isDirectory($viewPath)) {
            File::deleteDirectory($viewPath);
        }
    }

}
