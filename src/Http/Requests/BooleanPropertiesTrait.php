<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Http\Requests;

/**
 * If you want to use $this->prepareForValidation() in your request, this is still possible.
 * Change YourRequest as follows:
 *
 * use BooleanPropertiesTrait {
 *     prepareForValidation as traitPrepareForValidation;
 * }
 *
 * protected function prepareForValidation()
 * {
 *     // the stuff you want to do in YourRequest
 *     $this->traitPrepareForValidation();
 * }
 */
trait BooleanPropertiesTrait
{
    protected array $booleanProperties = [];

    protected function prepareForValidation(): void
    {
        $this->merge(collect($this->booleanProperties)->mapWithKeys(function (string $property) {
            return [$property => $this->has($property)];
        })->toArray());
    }
}