<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

abstract class AbstractFormRequest extends FormRequest
{
    use BooleanPropertiesTrait;

    protected array $booleanProperties = [];

    public function authorize() : bool
    {
        return Auth::check();
    }
}