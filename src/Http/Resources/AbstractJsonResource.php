<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

abstract class AbstractJsonResource extends JsonResource
{
    protected array $removeAttributes = [];

    /**
     * @param Request $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        return collect(parent::toArray($request))
            ->filter(function ($value, string $attribute) {
                return !in_array(needle: $attribute, haystack: $this->removeAttributes);
            })
            ->toArray();
    }
}