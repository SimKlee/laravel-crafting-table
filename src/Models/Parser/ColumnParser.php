<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Parser;

use Illuminate\Support\Collection;
use SimKlee\LaravelCraftingTable\Exceptions\MissingForeignKeyTypeDefinition;
use SimKlee\LaravelCraftingTable\Exceptions\MissingValidatorException;
use SimKlee\LaravelCraftingTable\Exceptions\MultipleDataTypeKeywordsFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\NoCastTypeForDataTypeException;
use SimKlee\LaravelCraftingTable\Exceptions\NoDataTypeKeywordFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\UnknownForeignKeyTypeDefinition;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;
use SimKlee\LaravelCraftingTable\Models\Validation\ArrayValidator;
use SimKlee\LaravelCraftingTable\Models\Validation\BooleanValidator;
use SimKlee\LaravelCraftingTable\Models\Validation\CarbonValidator;
use SimKlee\LaravelCraftingTable\Models\Validation\FloatValidator;
use SimKlee\LaravelCraftingTable\Models\Validation\IntValidator;
use SimKlee\LaravelCraftingTable\Models\Validation\StringValidator;
use SimKlee\LaravelCraftingTable\Models\Validation\ValidatorInterface;

class ColumnParser
{
    public const FOREIGN_KEY    = 'foreignKey';
    public const AUTO_INCREMENT = 'autoIncrement';

    private array            $keywords = [];
    private string           $definition;
    private ColumnDefinition $columnDefinition;

    private Collection $warnings;
    private Collection $errors;

    /**
     * @param string $name
     * @param string $definition
     *
     * @throws MissingForeignKeyTypeDefinition
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws UnknownForeignKeyTypeDefinition
     * @throws MissingValidatorException
     */
    public function __construct(string $name, string $definition)
    {
        $this->columnDefinition       = new ColumnDefinition();
        $this->columnDefinition->name = $name;
        $this->definition             = $definition;
        $this->keywords               = $this->normalizeKeywords(explode(separator: '|', string: $definition));

        $this->warnings = new Collection();
        $this->errors   = new Collection();

        $this->parse();
        $this->validate();
    }

    /**
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws MissingForeignKeyTypeDefinition
     * @throws UnknownForeignKeyTypeDefinition
     */
    private function parse(): void
    {
        $foreignKeyParser       = new ForeignKeyParser(
            columnDefinition: $this->columnDefinition,
            definition      : $this->definition
        );
        $this->columnDefinition = $foreignKeyParser->parse();

        try {
            $dataTypeParser                       = new DataTypeParser($this->keywords);
            $this->columnDefinition->dataType     = $dataTypeParser->getDataType();
            $this->columnDefinition->dataTypeCast = $dataTypeParser->getCastType();
        } catch (NoDataTypeKeywordFoundException $e) {
            if ($this->columnDefinition->foreignKey === false) {
                throw $e;
            }
        }

        $this->columnDefinition->unsigned      = $this->keywordExists('unsigned');
        $this->columnDefinition->nullable      = $this->keywordExists('nullable');
        $this->columnDefinition->autoIncrement = $this->keywordExists(['autoincrement', 'autoIncrement', 'ai']);
        $this->columnDefinition->length        = $this->getIntegerValueFromKeyword('length');
        $this->columnDefinition->precision     = $this->getIntegerValueFromKeyword('precision');
        $this->columnDefinition->default       = $this->getNormalizedDefaultValue();
        $this->columnDefinition->scale         = $this->getIntegerValueFromKeyword('scale');
    }

    private function normalizeKeywords(array $keywords): array
    {
        $normalizedKeywords = [];
        foreach ($keywords as $keyword) {
            $normalizedKeywords[] = match ($keyword) {
                'ai',
                'autoincrement' => 'autoIncrement',
                default         => $keyword,
            };
        }

        return $normalizedKeywords;
    }

    private function keywordExists(string|array $keywords): bool
    {
        return collect($this->keywords)->filter(function (string $keyword) use ($keywords) {
                return (is_array($keywords) && in_array(needle: $keyword, haystack: $keywords))
                       || $keyword === $keywords;
            })->count() > 0;
    }

    private function getValueFromKeyword(string $keyword): string|null
    {
        foreach ($this->keywords as $key) {
            if (str_starts_with(haystack: $key, needle: $keyword) && str_contains(haystack: $key, needle: ':')) {
                $value = explode(separator: ':', string: $key)[1];
                if (strtolower($value) === 'null') {
                    $value = 'NULL';
                }

                return $value;
            }
        }

        return null;
    }

    private function getIntegerValueFromKeyword(string $keyword): int|string|null
    {
        $value = $this->getValueFromKeyword($keyword);

        return !is_null($value) ? (int) $value : null;
    }

    private function getFloatValueFromKeyword(string $keyword): float|string|null
    {
        $value = $this->getValueFromKeyword($keyword);

        return !is_null($value) ? (float) $value : null;
    }

    private function getNormalizedDefaultValue(): string|int|bool|null|float
    {
        return match ($this->columnDefinition->dataTypeCast) {
            'Carbon'  => $this->normalizeCarbonDefaultValue(),
            'boolean' => $this->normalizeBooleanDefaultValue(),
            'int'     => $this->getIntegerValueFromKeyword('default'),
            'float'   => $this->getFloatValueFromKeyword('default'),
            default   => $this->getValueFromKeyword('default'),
        };
    }

    private function normalizeCarbonDefaultValue(): string|null
    {
        return match ($this->getValueFromKeyword('default')) {
            'current_timestamp',
            'currentTimestamp',
            'CURRENT_TIMESTAMP' => 'CURRENT_TIMESTAMP',
            default             => $this->getValueFromKeyword('default'),
        };
    }

    private function normalizeBooleanDefaultValue(): bool|string|null
    {
        return match ($this->getValueFromKeyword('default')) {
            'True', 'TRUE', '1', 'true'    => true,
            'False', 'FALSE', '0', 'false' => false,
            default                        => $this->getValueFromKeyword('default'),
        };
    }

    public function getColumnDefinition(): ColumnDefinition
    {
        return $this->columnDefinition;
    }

    /**
     * @throws MissingValidatorException
     */
    private function validate(): void
    {
        if ($this->columnDefinition->foreignKey) {
            // @TODO: validate fk after processing
            return;
        }

        /** @var ValidatorInterface $validator */
        $validator = match ($this->columnDefinition->dataTypeCast) {
            'int'     => new IntValidator($this->columnDefinition),
            'string'  => new StringValidator($this->columnDefinition),
            'Carbon'  => new CarbonValidator($this->columnDefinition),
            'array'   => new ArrayValidator($this->columnDefinition),
            'float'   => new FloatValidator($this->columnDefinition),
            'boolean' => new BooleanValidator($this->columnDefinition),
            default   => throw new MissingValidatorException(
                $this->columnDefinition->dataTypeCast ?? $this->columnDefinition->toString()
            ),
        };

        $validator->validate();

        if ($validator->hasWarnings()) {
            $validator->getWarnings()->each(function (string $warning, string $column) {
                $this->warnings->put(key: $column, value: $warning);
            });
        }

        if ($validator->hasErrors()) {
            $validator->getErrors()->each(function (string $error, string $column) {
                $this->errors->put(key: $column, value: $error);
            });
        }
    }

    public function getWarnings(): Collection
    {
        return $this->warnings;
    }

    public function getErrors(): Collection
    {
        return $this->errors;
    }
}
