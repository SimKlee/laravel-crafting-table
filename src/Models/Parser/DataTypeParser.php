<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Parser;

use SimKlee\LaravelCraftingTable\Exceptions\MultipleDataTypeKeywordsFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\NoCastTypeForDataTypeException;
use SimKlee\LaravelCraftingTable\Exceptions\NoDataTypeKeywordFoundException;

class DataTypeParser
{
    private ?string $dataType;

    /**
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoDataTypeKeywordFoundException
     */
    public function __construct(array $keywords)
    {
        $foundTypeKeywords = collect($keywords)->filter(function (string $keyword) {
            return !is_null($this->getType($keyword));
        });

        match ($foundTypeKeywords->count()) {
            1       => $this->setDataType($foundTypeKeywords->first()),
            0       => throw new NoDataTypeKeywordFoundException(implode(', ', $keywords)),
            default => throw new MultipleDataTypeKeywordsFoundException($foundTypeKeywords->implode(', ')),
        };
    }

    public function getDataType(): string
    {
        return $this->dataType;
    }

    /**
     * @throws NoCastTypeForDataTypeException
     */
    public function getCastType(): string
    {
        return match ($this->dataType) {
            'tinyInteger',
            'smallInteger',
            'mediumInteger',
            'integer',
            'bigInteger' => 'int',
            'string',
            'varchar',
            'char',
            'tinyText',
            'mediumText',
            'text'       => 'string',
            'json'       => 'array',
            'boolean'    => 'boolean',
            'decimal',
            'float'      => 'float',
            'date',
            'dateTime',
            'timestamp',
            'time',
            'year'       => 'Carbon',
            default      => throw new NoCastTypeForDataTypeException($this->dataType),
        };
    }

    private function getType(string $type): string|null
    {
        return match (strtolower($type)) {
            'tinyinteger', 'tinyint'     => 'tinyInteger',
            'smallinteger', 'smallint'   => 'smallInteger',
            'mediuminteger', 'mediumint' => 'mediumInteger',
            'integer', 'int'             => 'integer',
            'decimal'                    => 'decimal',
            'float'                      => 'float',
            'biginteger', 'bigint'       => 'bigInteger',
            'varchar', 'string'          => 'string',
            'tinytext'                   => 'tinyText',
            'mediumtext'                 => 'mediumText',
            'text'                       => 'text',
            'date'                       => 'date',
            'datetime'                   => 'dateTime',
            'boolean', 'bool'            => 'boolean',
            'char'                       => 'char',
            'json'                       => 'json',
            'timestamp'                  => 'timestamp',
            default                      => null,
        };
    }

    private function setDataType(string $keyword): void
    {
        $this->dataType = $this->getType($keyword);
    }
}
