<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Parser;

use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;

class Normalizer
{
    private ColumnDefinition $columnDefinition;
    private array            $keywords = [];

    public function __construct(ColumnDefinition $columnDefinition)
    {
        $this->columnDefinition = $columnDefinition;
    }

    private function normalizeKeywords(array $keywords): array
    {
        $normalizedKeywords = [];
        foreach ($keywords as $keyword) {
            $normalizedKeywords[] = match ($keyword) {
                'ai',
                'autoincrement' => 'autoIncrement',
                default         => $keyword,
            };
        }

        return $normalizedKeywords;
    }
}
