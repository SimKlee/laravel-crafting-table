<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Traits;

use SimKlee\LaravelCraftingTable\Models\AbstractModel;
use Illuminate\Support\Str;

trait UuidTrait
{
    public static function bootUuidTrait()
    {
        self::creating(function (AbstractModel $model) {
            $model->uuid = Str::uuid();
        });
    }

    public function getRouteKeyName() : string
    {
        return self::PROPERTY_UUID;
    }

    protected function getArrayableItems(array $values) : array
    {
        /** @var AbstractModel $this */
        if (!in_array('hidden', $this->hidden)) {
            $this->hidden[] = self::PROPERTY_ID;
        }

        return parent::getArrayableItems($values);
    }

    public static function findByUuid(string $uuid) : AbstractModel
    {
        return static::where(static::PROPERTY_UUID, $uuid)->first();
    }
}