<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use ReflectionClass;
use SimKlee\LaravelCraftingTable\Exceptions\ClassNotFoundException;

abstract class AbstractModel extends Model
{
    use HasFactory;

    public const TABLE = '';

    public const DELETED_AT = 'deleted_at';

    protected $perPage = 10;

    protected array $dateTypes = [];

    /**
     * @param Builder $query
     *
     * @return AbstractModelQuery
     * @throws ClassNotFoundException
     */
    public function newEloquentBuilder($query): AbstractModelQuery
    {
        $class = sprintf('\App\Queries\%sQuery', class_basename(static::class));

        if (class_exists($class) === false) {
            throw new ClassNotFoundException($class);
        }

        return new $class($query);
    }

    public static function repository(): ModelRepositoryInterface|null
    {
        $class = sprintf('\App\Repositories\%sRepository', class_basename(static::class));
        if (class_exists($class)) {
            return new $class();
        }

        return null;
    }

    /**
     * @throws ClassNotFoundException
     */
    public static function resource(Model $model): JsonResource
    {
        $class = sprintf('\App\Http\Resources\%sResource', class_basename(static::class));
        if (class_exists($class)) {
            return new $class($model);
        }

        throw new ClassNotFoundException($class);
    }

    /**
     * @throws ClassNotFoundException
     */
    public static function validation(): AbstractModelValidator
    {
        $class = sprintf('App\Models\Validators\%sValidator', class_basename(static::class));

        if (class_exists($class) === false) {
            throw new ClassNotFoundException($class);
        }

        return new $class();
    }

    public static function column(string $column, string|null $alias = null): string
    {
        if (is_null($alias)) {
            return sprintf('%s.%s', static::TABLE, $column);
        }

        return sprintf('%s.%s AS %s', static::TABLE, $column, $alias);
    }

    public function getModelName(): string
    {
        return class_basename($this);
    }

    public static function createFake(array $attributes = []): Model|AbstractModel
    {
        return static::factory()->create($attributes);
    }

    public static function makeFake(array $attributes = []): Model|AbstractModel
    {
        return static::factory()->make($attributes);
    }

    public static function createFakes(int $count, array $attributes = []): Collection
    {
        return static::factory()->count($count)->create($attributes);
    }

    public static function makeFakes(int $count, array $attributes = []): Collection
    {
        return static::factory()->count($count)->make($attributes);
    }

    public static function getModelAttributes(): Collection
    {
        return collect(Schema::getColumnListing(static::TABLE));
    }

    public static function hasAttribute($attribute): bool
    {
        return Schema::hasColumn(static::TABLE, $attribute);
    }

    public function getPropertyDateFormat(string $property): string|null
    {
        // @TODO: refac!!!
        if (isset($this->dateFormat[$property])) {
            return $this->dateFormat[$property];
        }

        return null;
    }

    public function getDateType(string $property): string|null
    {
        if (isset($this->dateTypes[$property])) {
            return $this->dateTypes[$property];
        }

        return null;
    }

    public function getDateTypeFormat(string $dateType): string
    {
        return match ($dateType) {
            'time'      => 'H:i',
            'date'      => 'Y-m-d',
            'datetime'  => 'Y-m-d H:i',
            'timestamp' => 'Y-m-d H:i',
            default     => throw new \Exception($dateType),
        };
    }

    public static function getProperties(): Collection
    {
        $reflection = new ReflectionClass(static::class);
        return collect($reflection->getConstants())
            ->filter(function (mixed $value, string $name) {
                return Str::startsWith(haystack: $name, needles: 'PROPERTY_');
            });
    }

    public static function getPropertyValues(string $property): Collection
    {
        $needle     = Str::upper($property) . '_';
        $reflection = new ReflectionClass(static::class);
        return collect($reflection->getConstants())
            ->filter(function (mixed $value, string $name) use ($needle) {
                return Str::startsWith(haystack: $name, needles: $needle);
            });
    }
}
