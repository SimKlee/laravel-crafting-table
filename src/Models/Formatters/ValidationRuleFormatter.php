<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Formatters;

use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;

class ValidationRuleFormatter
{
    private ColumnDefinition $columnDefinition;
    private bool             $guessAdditionalRules;
    private array            $additionalRulesContainingKeywords = [
        'uuid' => 'uuid',
        'url'  => 'url',
        'mail' => 'email',
    ];

    public function __construct(ColumnDefinition $columnDefinition, bool $guessAdditionalRules = true)
    {
        $this->columnDefinition     = $columnDefinition;
        $this->guessAdditionalRules = $guessAdditionalRules;
    }

    /**
     * @see https://laravel.com/docs/9.x/validation#available-validation-rules
     */
    public function validationRule(): string
    {
        $rules = new Collection();

        $rules->add(match ($this->columnDefinition->dataTypeCast) {
            'string'  => 'string',
            'int'     => 'integer',
            'float'   => 'numeric',
            'boolean' => 'boolean',
            'Carbon'  => 'date',
            default   => '',
        });

        $this->addAdditionalRules($rules);

        $required = $this->columnDefinition->nullable ? 'nullable' : 'required';
        if ($this->columnDefinition->dataTypeCast === 'boolean') {
            $required = 'sometimes';
        }

        $rules->add($required);
        
        if ($this->columnDefinition->length) {
            $rules->add('max:' . $this->columnDefinition->length);
        }

        return $rules->implode('|');
    }

    private function addAdditionalRules(Collection $rules): void
    {
        if ($this->guessAdditionalRules) {
            collect($this->additionalRulesContainingKeywords)->each(function (string $rule, string $keyword) use ($rules) {
                if (Str::contains(haystack: $keyword, needles: $this->columnDefinition->name)) {
                    $rules->add($rule);
                }
            })->toArray();
        }
    }
}