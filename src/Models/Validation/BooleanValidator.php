<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Validation;

class BooleanValidator extends AbstractValidator
{
    protected function requirements(): array
    {
        return [];
    }

    protected function defaults(): array
    {
        return [];
    }
}
