<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Validation;

use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;

class StringValidator extends AbstractValidator
{
    protected function requirements(): array
    {
        if (in_array($this->columnDefinition->dataType, ['string', 'char'])) {
            return [
                ColumnDefinition::LENGTH,
            ];
        }

        return [];
    }

    protected function defaults(): array
    {
        return [];
    }
}
