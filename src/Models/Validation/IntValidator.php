<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Validation;

class IntValidator extends AbstractValidator
{
    protected function requirements(): array
    {
        return [];
    }

    protected function defaults(): array
    {
        return [];
    }
}
