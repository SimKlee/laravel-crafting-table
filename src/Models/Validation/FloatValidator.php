<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Validation;

use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;

class FloatValidator extends AbstractValidator
{
    protected function requirements(): array
    {
        return [
            ColumnDefinition::PRECISION,
            ColumnDefinition::SCALE,
        ];
    }

    protected function defaults(): array
    {
        return [
            ColumnDefinition::PRECISION => 8,
            ColumnDefinition::SCALE     => 2,
        ];
    }
}
