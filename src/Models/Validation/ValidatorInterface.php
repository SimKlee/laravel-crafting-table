<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Validation;

use Illuminate\Support\Collection;

interface ValidatorInterface
{
    public function hasWarnings(): bool;

    public function hasErrors(): bool;

    public function getWarnings(): Collection;

    public function getErrors(): Collection;

    public function validate(): void;

}
