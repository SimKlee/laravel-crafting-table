<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Validation;

use Illuminate\Support\Collection;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;

abstract class AbstractValidator implements ValidatorInterface
{
    protected ColumnDefinition $columnDefinition;
    protected Collection       $errors;
    protected Collection       $warnings;

    public function __construct(ColumnDefinition $columnDefinition)
    {
        $this->columnDefinition = $columnDefinition;
        $this->errors           = new Collection();
        $this->warnings         = new Collection();
    }

    abstract protected function requirements(): array;

    abstract protected function defaults(): array;

    public function validate(): void
    {
        collect($this->requirements())->each(function (string $key) {
            if (is_null($this->columnDefinition->{$key})) {
                $defaults = $this->defaults();
                if (isset($defaults[$key])) {
                    $this->setDefault(key: $key);
                } else {
                    $this->errors->put(key: $key, value: sprintf('Missing %s. No default value defined.', $key));
                }
            }
        });
    }

    public function hasWarnings(): bool
    {
        return $this->warnings->count() > 0;
    }

    public function hasErrors(): bool
    {
        return $this->errors->count() > 0;
    }

    public function getWarnings(): Collection
    {
        return $this->warnings;
    }

    public function getErrors(): Collection
    {
        return $this->errors;
    }

    protected function getDefault(string $key): mixed
    {
        return $this->defaults()[$key] ?? null;
    }

    protected function setDefault(string $key): void
    {
        $this->columnDefinition->{$key} = $this->getDefault($key);
        $this->warnings->put(key: $key, value: sprintf('Missing %s. Set to default: %s', $key, $this->getDefault($key)));
    }
}
