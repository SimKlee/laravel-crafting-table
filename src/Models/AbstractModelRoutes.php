<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models;

abstract class AbstractModelRoutes
{
    abstract public static function apiRoutes(): void;

    abstract public static function webRoutes(): void;
}