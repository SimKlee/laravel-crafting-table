<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models;

abstract class AbstractModelValidator
{
    public const CAST_STRING  = 'string';
    public const CAST_INTEGER = 'int';
    public const CAST_FLOAT   = 'float';
    public const CAST_BOOLEAN = 'bool';

    public const RULE_NULLABLE = 'nullable';
    public const RULE_REQUIRED = 'required';
    public const RULE_TYPE     = 'type';
    public const RULE_MAX      = 'max';
    public const RULE_MIN      = 'min';

    public const TYPE_ALPHA         = 'alpha';
    public const TYPE_ALPHA_DASH    = 'alpha_dash';
    public const TYPE_ALPHA_NUMERIC = 'alpha_numeric';
    public const TYPE_ARRAY         = 'array';
    public const TYPE_BOOLEAN       = 'boolean';
    public const TYPE_DATE          = 'date';
    public const TYPE_DIGITS        = 'digits';
    public const TYPE_EMAIL         = 'email';
    public const TYPE_FILE          = 'file';
    public const TYPE_INTEGER       = 'integer';
    public const TYPE_JSON          = 'json';
    public const TYPE_NUMERIC       = 'numeric';
    public const TYPE_PASSWORD      = 'password';
    public const TYPE_STRING        = 'string';
    public const TYPE_TIMEZONE      = 'timezone';
    public const TYPE_URL           = 'url';
    public const TYPE_UUID          = 'uuid';

    protected array $types = [
        self::TYPE_ALPHA,
        self::TYPE_ALPHA_DASH,
        self::TYPE_ALPHA_NUMERIC,
        self::TYPE_ARRAY,
        self::TYPE_BOOLEAN,
        self::TYPE_DATE,
        self::TYPE_DIGITS,
        self::TYPE_EMAIL,
        self::TYPE_FILE,
        self::TYPE_INTEGER,
        self::TYPE_JSON,
        self::TYPE_NUMERIC,
        self::TYPE_PASSWORD,
        self::TYPE_STRING,
        self::TYPE_TIMEZONE,
        self::TYPE_URL,
        self::TYPE_UUID,
    ];

    protected array $rules  = [];
    protected array $values = [];

    public function __construct()
    {
        foreach ($this->getRules() as $column => $rules) {
            if (is_string($rules)) {
                $this->rules[ $column ] = explode(separator: '|', string: $rules);
                if ($this->type($column) === self::TYPE_ARRAY) {
                    $this->values[ $column ] = explode(separator: ',', string: $this->getRuleValue(column: $column, rule: self::TYPE_ARRAY));
                }
            }
        }
    }

    public function type($column): string|null
    {
        if (isset($this->rules[ $column ])) {
            foreach ($this->rules[ $column ] as $rule) {
                if (in_array(needle: $rule, haystack: $this->types)) {
                    return $rule;
                }

                if (str_starts_with(haystack: $rule, needle: 'array')) {
                    return 'array';
                }
            }
        }

        return null;
    }

    public function required($column): bool|null
    {
        if (isset($this->rules[ $column ])) {
            return in_array(needle: self::RULE_REQUIRED, haystack: $this->rules[ $column ]);
        }

        return null;
    }

    public function nullable($column): bool|null
    {
        if (isset($this->rules[ $column ])) {
            return in_array(needle: self::RULE_NULLABLE, haystack: $this->rules[ $column ]);
        }

        return null;
    }

    public function max($column): int|null
    {
        return $this->getRuleValue(column: $column, rule: self::RULE_MAX, cast: self::CAST_INTEGER);
    }

    public function min($column): int|null
    {
        return $this->getRuleValue(column: $column, rule: self::RULE_MIN, cast: self::CAST_INTEGER);
    }

    private function getRuleValue(string $column, $rule, string $cast = self::CAST_STRING): mixed
    {
        if (isset($this->rules[ $column ])) {
            foreach ($this->rules[ $column ] as $current) {
                if (str_starts_with(haystack: $current, needle: $rule)) {
                    [, $value] = explode(separator: ':', string: $current);

                    return match ($cast) {
                        self::CAST_INTEGER => (int)$value,
                        self::CAST_FLOAT   => (float)$value,
                        self::CAST_BOOLEAN => (bool)$value,
                        default            => $value,
                    };
                }
            }
        }

        return null;
    }

    public function values(string $column): array
    {
        if (isset($this->values[ $column ])) {
            return $this->values[ $column ];
        }

        return [];
    }

    abstract public function getRules(): array;
}
