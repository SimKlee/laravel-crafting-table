<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Definitions;

class DefinitionHelper
{
    public static function hasOne(string $model): array
    {
        return self::relation('HasOne', $model);
    }

    public static function hasMany(string $model): array
    {
        return self::relation('HasMany', $model);
    }

    public static function belongsTo(string $model): array
    {
        return self::relation('BelongsTo', $model);
    }

    public static function belongsToMany(string $model, string $pivot): array
    {
        return self::relation('BelongsToMany', $model, $pivot);
    }

    public static function hasManyThrough(string $model, string $pivot): array
    {
        return self::relation('HasManyThrough', $model, $pivot);
    }

    private static function relation(string $type, string $model, string $pivot = null): array
    {
        return [
            'type'  => $type,
            'model' => $model,
            'pivot' => $pivot,
        ];
    }
}
