<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Definitions;

use Illuminate\Support\Collection;
use SimKlee\LaravelCraftingTable\Exceptions\UnknownRelationTypeException;
use SimKlee\LaravelCraftingTable\Generators\Formatters\RelationFormatter;

class RelationDefinition
{
    public const TYPE_HAS_ONE          = 'HasOne';
    public const TYPE_HAS_ONE_THROUGH  = 'HasOneThrough';
    public const TYPE_HAS_MANY         = 'HasMany';
    public const TYPE_HAS_MANY_THROUGH = 'HasManyThrough';
    public const TYPE_BELONGS_TO       = 'BelongsTo';
    public const TYPE_BELONGS_TO_MANY  = 'BelongsToMany';

    public string  $name;
    public string  $type;
    public string  $model;
    public ?string $pivot = null;

    /**
     * @throws UnknownRelationTypeException
     */
    public function __construct(string $name, string $type, string $model, string $pivot = null)
    {
        if (!in_array($type, [
            self::TYPE_HAS_ONE,
            self::TYPE_HAS_ONE_THROUGH,
            self::TYPE_HAS_MANY,
            self::TYPE_HAS_MANY_THROUGH,
            self::TYPE_BELONGS_TO,
            self::TYPE_BELONGS_TO_MANY,
        ])) {
            throw new UnknownRelationTypeException(sprintf('%s [%s]', $type, $name));
        }

        $this->name  = $name;
        $this->type  = $type;
        $this->model = $model;
        $this->pivot = $pivot;
    }

    /**
     * @throws UnknownRelationTypeException
     */
    public static function fromArray(string $name, array $definition): RelationDefinition
    {
        return new self(
            $name,
            $definition['type'],
            $definition['model'],
            $definition['pivot'] ?? null
        );
    }

    public function getUsedClasses(): Collection
    {
        $classes = [
            sprintf('Illuminate\Database\Eloquent\Relations\%s', $this->type),
            sprintf('App\Models\%s', $this->model),
        ];

        if (!is_null($this->pivot)) {
            $classes[] = sprintf('App\Models\%s', $this->pivot);
        }

        return collect($classes);
    }

    public function getFormatter(): RelationFormatter
    {
        return new RelationFormatter($this);
    }
}