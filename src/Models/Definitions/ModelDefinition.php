<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models\Definitions;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SimKlee\LaravelCraftingTable\Exceptions\MissingForeignKeyTypeDefinition;
use SimKlee\LaravelCraftingTable\Exceptions\MissingValidatorException;
use SimKlee\LaravelCraftingTable\Exceptions\MultipleDataTypeKeywordsFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\NoCastTypeForDataTypeException;
use SimKlee\LaravelCraftingTable\Exceptions\NoDataTypeKeywordFoundException;
use SimKlee\LaravelCraftingTable\Exceptions\UnknownForeignKeyTypeDefinition;
use SimKlee\LaravelCraftingTable\Models\AbstractModel;
use SimKlee\LaravelCraftingTable\Models\Parser\ColumnParser;

class ModelDefinition
{
    private array                $definition;
    public ?string               $model;
    public ?string               $namespace;
    public ?string               $table;
    public Collection            $columns;
    public array                 $values;
    public array                 $defaults;
    public bool                  $timestamps           = false;
    public bool                  $softDelete           = false;
    public bool                  $uuid                 = false;
    public array                 $lookup;
    /** @var Collection|RelationDefinition[]  */
    public Collection            $relations;
    public Collection            $traits;
    public Collection            $uses;
    public ?ForeignKeyDefinition $foreignKeyDefinition = null;

    /**
     * @throws MissingForeignKeyTypeDefinition
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws UnknownForeignKeyTypeDefinition
     * @throws MissingValidatorException
     */
    public function __construct(string $model, array $definition = [])
    {
        $this->model      = $model;
        $this->definition = $definition;
        $this->columns    = new Collection();
        $this->traits     = new Collection();
        $this->uses       = new Collection();
        $this->relations  = new Collection();

        $this->processDefinitions();
    }

    /**
     * @throws MissingForeignKeyTypeDefinition
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws UnknownForeignKeyTypeDefinition
     * @throws MissingValidatorException
     */
    private function fromArray(): void
    {
        $this->namespace  = $this->fromDefinitionArrayIfExists('namespace');
        $this->table      = $this->fromDefinitionArrayIfExists('table');
        $this->values     = $this->fromDefinitionArrayIfExists('values', []);
        $this->defaults   = $this->fromDefinitionArrayIfExists('defaults', []);
        $this->timestamps = $this->fromDefinitionArrayIfExists('timestamps', false);
        $this->softDelete = $this->fromDefinitionArrayIfExists('softDelete', false);
        $this->uuid       = $this->fromDefinitionArrayIfExists('uuid', false);
        if ($this->uuid) {
            $this->addColumn('uuid', 'char|length:40');
        }

        // @TODO: refac to object like RelationDefinition
        $this->lookup = $this->fromDefinitionArrayIfExists('lookup', []);

        $this->processRelations();

        collect($this->fromDefinitionArrayIfExists('columns'))->each(function (string $definition, string $column) {
            $this->addColumn($column, $definition);
        });
    }

    private function processRelations(): void
    {
        $this->relations = collect($this->fromDefinitionArrayIfExists('relations', []))
            ->map(function (array $definition, string $name) {
                $relationDefinition = RelationDefinition::fromArray($name, $definition);
                $relationDefinition->getUsedClasses()
                                   ->each(function (string $class) {
                                       $this->addUses($class);
                                   });

                return $relationDefinition;
            });
    }

    public function getColumnsWithForeignKey(): Collection
    {
        return $this->columns->filter(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->foreignKey;
        });
    }

    /**
     * @throws MissingForeignKeyTypeDefinition
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws UnknownForeignKeyTypeDefinition
     * @throws MissingValidatorException
     */
    private function processDefinitions(): void
    {
        $this->fromArray();

        $this->processTimestamps();
        $this->processSoftDelete();
    }

    private function fromDefinitionArrayIfExists(string $key, mixed $default = null): mixed
    {
        if (isset($this->definition[$key])) {
            return $this->definition[$key];
        }

        return $default;
    }

    public function hasDates(): bool
    {
        return collect($this->columns)->filter(function (ColumnDefinition $columnDefinition) {
                return $columnDefinition->dataTypeCast === 'Carbon';
            })->count() > 0;
    }

    public function getDates(): Collection
    {
        return collect($this->columns)->filter(function (ColumnDefinition $columnDefinition) {
            return $columnDefinition->dataTypeCast === 'Carbon';
        });
    }

    /**
     * @throws MissingForeignKeyTypeDefinition
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws UnknownForeignKeyTypeDefinition
     * @throws MissingValidatorException
     */
    private function processTimestamps(): void
    {
        if ($this->timestamps) {
            $this->addColumn(AbstractModel::CREATED_AT, 'timestamp|default:CURRENT_TIMESTAMP');
            $this->addColumn(AbstractModel::UPDATED_AT, 'timestamp|nullable');
        }
    }

    /**
     * @throws MissingForeignKeyTypeDefinition
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws UnknownForeignKeyTypeDefinition
     * @throws MissingValidatorException
     */
    private function processSoftDelete(): void
    {
        if ($this->softDelete) {
            $this->addColumn(AbstractModel::DELETED_AT, 'timestamp|nullable');
            $this->addTrait(\Illuminate\Database\Eloquent\SoftDeletes::class);
        }
    }

    /**
     * @throws MultipleDataTypeKeywordsFoundException
     * @throws NoCastTypeForDataTypeException
     * @throws NoDataTypeKeywordFoundException
     * @throws MissingForeignKeyTypeDefinition
     * @throws UnknownForeignKeyTypeDefinition
     * @throws MissingValidatorException
     */
    private function addColumn(string $name, string $definition): void
    {
        if ($this->columns->has($name) === false) {
            $columnDefinition = (new ColumnParser($name, $definition))->getColumnDefinition();
            $this->columns->put($name, $columnDefinition);
            if ($columnDefinition->dataTypeCast === 'Carbon') {
                $this->addUses(Carbon::class);
            }
        }
    }

    public function getColumn(string $name): ColumnDefinition
    {
        return $this->columns->get($name);
    }

    public function getFilteredColumns(array $excluded = []): Collection
    {
        return $this->columns->filter(function (ColumnDefinition $columnDefinition) use ($excluded) {
            return !in_array(needle: $columnDefinition->name, haystack: $excluded);
        });
    }

    public function getModelVarName(bool $snake = false): string
    {
        if ($snake) {
            return lcfirst(Str::snake($this->model));
        }

        return lcfirst($this->model);
    }

    public function getModelPublicId(): string
    {
        return $this->uuid ? 'uuid' : 'id';
    }

    private function addTrait(string $class): void
    {
        if ($this->traits->contains($class) === false) {
            $this->traits->add(class_basename($class));
            $this->addUses($class);
        }
    }

    private function addUses(string $class): void
    {
        if ($this->uses->contains($class) === false) {
            $this->uses->add($class);
        }
    }

    public function getCasts(string $type = null): array
    {
        if (is_null($type)) {
            $filtered = $this->columns->filter(function (ColumnDefinition $columnDefinition) {
                return !in_array(needle: $columnDefinition->dataTypeCast, haystack: ['string', 'Carbon']);
            });
        } else {
            $filtered = $this->columns->filter(function (ColumnDefinition $columnDefinition) use ($type) {
                return $columnDefinition->dataTypeCast === $type;
            });
        }

        return $filtered->mapWithKeys(function (ColumnDefinition $columnDefinition) {
            return [$columnDefinition->name => $columnDefinition->dataTypeCast];
        })->toArray();
    }

    public function lookupLabel(): string
    {
        if (isset($this->lookup['label'])) {
            return $this->lookup['label'];
        }

        return 'id';
    }

}
