<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Models;

use Illuminate\Contracts\Database\Eloquent\Castable;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use ReflectionClass;
use ReflectionProperty;

abstract class AbstractModelJsonProperty implements Castable, CastsAttributes
{
    protected bool $ignoreNullValues = false;

    public function __construct(array $values = [])
    {
        collect($values)->each(function ($value, string $key) {
            $this->{$key} = $value;
        });
    }

    public static function castUsing(array $arguments): string|static
    {
        return new static();
    }

    public function get($model, string $key, $value, array $attributes): AbstractModelJsonProperty
    {
        if (is_null($value)) {
            return new static();
        }

        return new static(json_decode($value, true));
    }

    public function set($model, string $key, $value, array $attributes): string|null
    {
        if (is_null($value)) {
            return null;
        }

        return $value->toJson();
    }

    public function toJson(): string|false
    {
        return json_encode($this->toArray());
    }

    public function toArray(): array
    {
        return collect((new ReflectionClass($this))->getProperties(ReflectionProperty::IS_PUBLIC))
            ->filter(function (ReflectionProperty $property) {
                return $this->ignoreNullValues === false || !is_null($this->{$property->name});
            })
            ->mapWithKeys(function (ReflectionProperty $property) {
                return [$property->name => $this->{$property->name}];
            })
            ->toArray();
    }

}