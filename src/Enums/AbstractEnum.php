<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Enums;

use phpDocumentor\Reflection\Types\Static_;
use ReflectionClass;
use ReflectionClassConstant;
use ValueError;

abstract class AbstractEnum
{
    public string|int $value;

    public function __construct(string|int $value)
    {
        if (!in_array($value, static::cases())) {
            throw new ValueError($value);
        }

        $this->value = $value;
    }

    public static function cases(): array
    {
        return collect((new ReflectionClass(static::class))->getConstants(ReflectionClassConstant::IS_PUBLIC))
            ->map(function (string|int $value, string $name) {
                return $value;
            })->toArray();
    }

    /**
     * @throws ValueError
     */
    public static function from(string|int $value): AbstractEnum
    {
        return new static($value);
    }

    public static function tryFrom(string|int $value): AbstractEnum|null
    {
        try {
            self::from($value);
        } catch (ValueError $e) {
            return null;
        }
    }
}