<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Tests;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Schema;
use SimKlee\LaravelCraftingTable\Models\AbstractModel;
use SimKlee\LaravelCraftingTable\Models\AbstractModelQuery;
use SimKlee\LaravelCraftingTable\Models\AbstractModelRepository;
use SimKlee\LaravelCraftingTable\Models\ModelQueryInterface;
use SimKlee\LaravelCraftingTable\Models\ModelRepositoryInterface;
use Tests\CreatesApplication;

abstract class ModelTestCase extends TestCase
{
    use RefreshDatabase;
    use CreatesApplication;

    abstract protected function getModelClass(): string;

    abstract protected function getModelProperties(): Collection;

    public function testInstanceOfModel(): void
    {
        $className = $this->getModelClass();
        $this->assertInstanceOf(expected: $className, actual: new $className());
        $this->assertContains(needle: AbstractModel::class, haystack: class_parents($className));
    }

    public function testFactoryMake()
    {
        $className = $this->getModelClass();
        $model     = call_user_func(callback: [$className, 'makeFake']);
        $this->assertInstanceOf(expected: $className, actual: $model);
    }

    public function testFactoryCreate()
    {
        $className = $this->getModelClass();
        $model     = call_user_func(callback: [$className, 'createFake']);
        $this->assertInstanceOf(expected: $className, actual: $model);
    }

    public function testColumnCount()
    {
        $this->assertCount(count($this->getModelProperties()), Schema::getColumnListing($this->getModelClass()::TABLE));
    }

    public function testModelPropertiesEqualsDatabaseColumns(): void
    {
        $class   = $this->getModelClass();
        $columns = Schema::getColumnListing($class::TABLE);
        $this->getModelProperties()->each(function (string $column) use ($columns) {
            $this->assertContains(
                needle  : $column,
                haystack: $columns,
                message : 'Missing database column: ' . $column
            );
        });
    }

    public function testDatabaseColumnsEqualsModelProperties(): void
    {
        collect(Schema::getColumnListing($this->getModelClass()::TABLE))
            ->each(function (string $column) {
                $this->assertContains(
                    needle  : $column,
                    haystack: $this->getModelProperties(),
                    message : 'Missing model property: ' . $column
                );
            });
    }

    public function testModelQuery(): void
    {
        $query = call_user_func(callback: [$this->getModelClass(), 'query']);
        $this->assertInstanceOf(expected: Builder::class, actual: $query);
        $this->assertInstanceOf(expected: AbstractModelQuery::class, actual: $query);
        $this->assertInstanceOf(
            expected: sprintf('App\Queries\%sQuery', class_basename($this->getModelClass())),
            actual  : $query
        );
        $this->assertContains(needle: ModelQueryInterface::class, haystack: class_implements($query));
    }

    public function testModelRepository(): void
    {
        $repository = call_user_func(callback: [$this->getModelClass(), 'repository']);
        $this->assertInstanceOf(expected: AbstractModelRepository::class, actual: $repository);
        $this->assertContains(needle: ModelRepositoryInterface::class, haystack: class_implements($repository));
    }
}
