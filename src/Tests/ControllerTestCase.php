<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Tests;

use Illuminate\Foundation\Testing\TestCase;
use Tests\CreatesApplication;

abstract class ControllerTestCase extends TestCase
{
    use CreatesApplication;
    
    protected function login(): void
    {
        auth()->login(\App\Models\User::factory()->create());
    }
}
