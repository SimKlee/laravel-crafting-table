<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Support;

class SystemCommand
{
    private string $command;
    private array  $output     = [];
    private int    $resultCode = 0;

    public function __construct(string $command)
    {
        $this->command = $command;
    }

    public static function create(string $command): SystemCommand
    {
        return new SystemCommand($command);
    }

    public function run(bool $quiet = true): int
    {
        $command = $this->command;

        if ($quiet) {
            $command .= ' > /dev/null 2>&1';
        }

        exec(command: $command, output: $this->output, result_code: $this->resultCode);

        return $this->resultCode;
    }

    public function getOutput(): array
    {
        return $this->output;
    }
}
