<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;
use SimKlee\LaravelCraftingTable\Models\Traits\UuidTrait;

class ModelGenerator extends AbstractGenerator
{
    protected static string $generator = 'model';
    protected string        $template  = 'model';

    protected function beforeWrite(): void
    {
        $this->setExtends(config('craftingtable.model.extends'));
        $this->addUse(\Illuminate\Support\Collection::class);
        $this->addUse(sprintf(
            '%s\%sQuery',
            call_user_func([ModelQueryGenerator::class, 'namespace']),
            $this->modelDefinition->model
        ));
        $this->addUse(sprintf(
            '%s\%sRepository',
            call_user_func([ModelRepositoryGenerator::class, 'namespace']),
            $this->modelDefinition->model
        ));
        $this->addUses($this->uses);

        if ($this->modelDefinition->hasDates()) {
            $this->addUse(Carbon::class);
        }

        if ($this->modelDefinition->softDelete) {
            $this->addUse(SoftDeletes::class);
        }

        $columnsWithForeignKeys = $this->modelDefinition->getColumnsWithForeignKey();

        if ($columnsWithForeignKeys->count() > 0) {
            $this->addUse(BelongsTo::class);
        }

        if ($this->modelDefinition->uuid) {
            $this->addUse(UuidTrait::class);
            $this->addTrait(class_basename(UuidTrait::class));
        }

        $this->addUses($this->modelDefinition->uses->filter(function (string $class) {
            $parts = explode('\\', $class);
            array_pop($parts);
            return implode('\\', $parts) !== config('craftingtable.model.namespace');
        }));

        $columnsWithForeignKeys->each(function (ColumnDefinition $columnDefinition) {
            #$this->addUse(sprintf('App\Models\%s', $columnDefinition->foreignKeyModel));
        });
    }

}
