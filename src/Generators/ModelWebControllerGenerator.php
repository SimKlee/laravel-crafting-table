<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelWebControllerGenerator extends AbstractGenerator
{
    protected static string $generator = 'web_controller';
    protected string        $template  = 'model_controller';

    protected function beforeWrite(): void
    {
        $this->addUses([
            \Illuminate\Contracts\View\View::class,
            \Illuminate\Contracts\View\Factory::class,
            \Illuminate\Http\RedirectResponse::class,
            \Illuminate\Http\Request::class,
            \Illuminate\Support\Str::class,
            sprintf(
                '%s\%sStoreRequest',
                call_user_func([ModelRequestGenerator::class, 'namespace']),
                $this->modelDefinition->model
            ),
            sprintf(
                '%s\%sUpdateRequest',
                call_user_func([ModelRequestGenerator::class, 'namespace']),
                $this->modelDefinition->model
            ),
            sprintf(
                '%s\%s',
                call_user_func([ModelGenerator::class, 'namespace']),
                $this->modelDefinition->model
            ),
        ]);
    }

}
