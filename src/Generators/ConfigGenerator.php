<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

use Illuminate\Support\Facades\File;

class ConfigGenerator
{
    public const MODUS_OVERRIDE = 0;
    public const MODUS_MERGE    = 1;

    private array $values = [];

    public function __construct(array $values = [])
    {
        $this->values = $values;
    }

    public function setValue(string $key, mixed $value): void
    {
        $this->values[$key] = $value;
    }

    public function write(string $file, int $modus = self::MODUS_OVERRIDE): bool|int
    {
        if ($modus === self::MODUS_MERGE && File::exists($file)) {
            $this->values = array_merge(include $file, $this->values);
        }

        return File::put($file, "<?php\nreturn " . var_export($this->values, true) . ';');
    }
}