<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelRepositoryGenerator extends AbstractGenerator
{
    protected static string $generator = 'repository';
    protected string $template  = 'model_repository';

    protected function beforeWrite(): void
    {
        $this->setExtends(config('craftingtable.repository.extends'));
        $this->addUse(sprintf(
            '%s\%s',
            call_user_func([ModelGenerator::class, 'namespace']),
            $this->modelDefinition->model
        ));
    }
}
