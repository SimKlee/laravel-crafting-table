<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelControllerTestGenerator extends AbstractGenerator
{
    protected static string $generator = 'web_controller_test';
    protected string        $template  = 'model_controller_test';

    protected function beforeWrite(): void
    {
        $this->setExtends(config('craftingtable.web_controller_test.extends'));
        $this->addUse(\Carbon\Carbon::class);
        $this->addUse(sprintf(
            '%s\%s',
            call_user_func([ModelGenerator::class, 'namespace']),
            $this->modelDefinition->model
        ));
    }

}
