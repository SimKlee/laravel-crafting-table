<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelMigrationGenerator extends AbstractGenerator
{
    protected static string $generator = 'migration';
    protected string        $template  = 'model_migration';

    protected function beforeWrite(): void
    {
        $this->setExtends(\SimKlee\LaravelCraftingTable\Models\ModelMigration::class);
        $this->addUses([
            \Illuminate\Database\Schema\Blueprint::class,
            \Illuminate\Support\Facades\Schema::class,
            sprintf('%s\%s', call_user_func([ModelGenerator::class, 'namespace']), $this->modelDefinition->model),
        ]);
    }

}
