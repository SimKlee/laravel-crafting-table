<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators\Formatters;

use SimKlee\LaravelCraftingTable\Exceptions\MissingMethodInMapForFormatterException;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;
use SimKlee\LaravelCraftingTable\Support\StringBuffer;
use Illuminate\Support\Str;

class ColumnFakerFormatter
{
    private string           $model;
    private ColumnDefinition $columnDefinition;

    public function __construct(string $model, ColumnDefinition $columnDefinition)
    {
        $this->model            = $model;
        $this->columnDefinition = $columnDefinition;
    }

    public static function create(string $model, ColumnDefinition $columnDefinition): ColumnFakerFormatter
    {
        return new ColumnFakerFormatter(model: $model, columnDefinition: $columnDefinition);
    }

    public function toString(): string
    {
        if ($this->columnDefinition->foreignKey) {
            return sprintf('\App\Models\%s::inRandomOrder()->take(1)->first()->id', $this->columnDefinition->foreignKeyModel);
        }

        $method = config(sprintf('models.%s.factory.%s', $this->model, $this->columnDefinition->name));
        if ($method) {
            return sprintf('$this->faker->%s', $method);
        }

        return 'null';
    }

    private function handleTypeInt(StringBuffer $buffer): StringBuffer
    {
        $method = $this->methodMap[$this->columnDefinition->dataType];

        return $buffer->appendIf(condition: $this->columnDefinition->unsigned, string: 'unsigned')
                      ->appendIf(
                          condition: $this->columnDefinition->unsigned,
                          string   : Str::ucfirst($method),
                          else     : $method
                      )
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )
                      ->appendIf(
                          condition: $this->columnDefinition->autoIncrement,
                          string   : ', autoIncrement: true'
                      )->append(')');
    }

    private function handleTypeString(StringBuffer $buffer): StringBuffer
    {
        return $buffer->append($this->methodMap[$this->columnDefinition->dataType])
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )
                      ->appendIfNotNull(
                          value : $this->columnDefinition->length,
                          string: ', length: ' . $this->columnDefinition->length
                      )->append(')');
    }

    private function handleTypeCarbon(StringBuffer $buffer): StringBuffer
    {
        return $buffer->append($this->methodMap[$this->columnDefinition->dataType])
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )->append(')');
    }

    private function handleTypeBoolean(StringBuffer $buffer): StringBuffer
    {
        return $buffer->append($this->methodMap[$this->columnDefinition->dataType])
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )->append(')');
    }

    private function handleTypeFloat(StringBuffer $buffer): StringBuffer
    {
        return $buffer->append($this->methodMap[$this->columnDefinition->dataType])
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )
                      ->appendIfNotNull(
                          value : $this->columnDefinition->length,
                          string: ', total: ' . $this->columnDefinition->length
                      )
                      ->appendIfNotNull(
                          value : $this->columnDefinition->precision,
                          string: ', places: ' . $this->columnDefinition->precision
                      )
                      ->appendIf(
                          condition: $this->columnDefinition->unsigned,
                          string   : ', unsigned: true',
                          else     : ', unsigned: false'
                      )->append(')');
    }

    private function addDefault(StringBuffer $buffer): StringBuffer
    {
        if (!is_null($this->columnDefinition->default)) {
            if ($this->columnDefinition->default === 'CURRENT_TIMESTAMP') {
                $buffer->append('->useCurrent()');
            } elseif (is_bool($this->columnDefinition->default)) {
                $buffer->appendFormatted('->default(%s)', $this->columnDefinition->default === true ? 'true' : 'false');
            } elseif (is_string($this->columnDefinition->default)) {
                $buffer->appendIf(
                    condition: $this->columnDefinition->default === 'NULL',
                    string   : '->default(null)',
                    else     : sprintf('->default(%s)', sprintf("'%s'", $this->columnDefinition->default))
                );
            } else {
                $buffer->appendFormatted('->default(%s)', $this->columnDefinition->default);
            }
        }

        return $buffer;
    }
}
