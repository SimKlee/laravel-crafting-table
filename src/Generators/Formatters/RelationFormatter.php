<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators\Formatters;

use SimKlee\LaravelCraftingTable\Exceptions\UnknownRelationTypeException;
use SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition;

class RelationFormatter
{
    private RelationDefinition $relationDefinition;

    public function __construct(RelationDefinition $relationDefinition)
    {
        $this->relationDefinition = $relationDefinition;
    }

    /**
     * @throws UnknownRelationTypeException
     */
    public function toString(): string
    {
        $template = match ($this->relationDefinition->type) {
            RelationDefinition::TYPE_HAS_ONE          => 'crafting-table::relations.has_one',
            RelationDefinition::TYPE_HAS_ONE_THROUGH  => 'crafting-table::relations.has_one_through',
            RelationDefinition::TYPE_HAS_MANY         => 'crafting-table::relations.has_many',
            RelationDefinition::TYPE_HAS_MANY_THROUGH => 'crafting-table::relations.has_many_through',
            RelationDefinition::TYPE_BELONGS_TO       => 'crafting-table::relations.belongs_to',
            RelationDefinition::TYPE_BELONGS_TO_MANY  => 'crafting-table::relations.belongs_to_many',
            default                                   => throw new UnknownRelationTypeException(
                sprintf('%s [%s]', $this->relationDefinition->type, $this->relationDefinition->name)
            ),
        };

        return view($template)
            ->with('relation', $this->relationDefinition)
            ->render();
    }
}