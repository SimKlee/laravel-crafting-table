<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators\Formatters;

use SimKlee\LaravelCraftingTable\Exceptions\MissingMethodInMapForFormatterException;
use SimKlee\LaravelCraftingTable\Exceptions\NoCastTypeForDataTypeException;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;
use SimKlee\LaravelCraftingTable\Support\StringBuffer;
use Illuminate\Support\Str;

class ColumnMigrationFormatter
{
    private string           $model;
    private ColumnDefinition $columnDefinition;

    private array $methodMap = [
        'tinyInteger'   => 'tinyInteger',
        'smallInteger'  => 'smallInteger',
        'mediumInteger' => 'mediumInteger',
        'integer'       => 'integer',
        'bigInteger'    => 'bigInteger',
        'string'        => 'string',
        'char'          => 'char',
        'date'          => 'date',
        'time'          => 'time',
        'dateTime'      => 'dateTime',
        'timestamp'     => 'timestamp',
        'decimal'       => 'decimal',
        'boolean'       => 'boolean',
        'text'          => 'text',
        'json'          => 'json',
    ];

    /**
     * @throws MissingMethodInMapForFormatterException
     */
    public function __construct(string $model, ColumnDefinition $columnDefinition)
    {
        $this->model            = $model;
        $this->columnDefinition = $columnDefinition;

        if (!isset($this->methodMap[$columnDefinition->dataType])) {
            throw new MissingMethodInMapForFormatterException(
                sprintf('Unknown migration method for data type "%s"', $columnDefinition->dataType)
            );
        }
    }

    /**
     * @throws MissingMethodInMapForFormatterException
     */
    public static function create(string $model, ColumnDefinition $columnDefinition): ColumnMigrationFormatter
    {
        return new ColumnMigrationFormatter(model: $model, columnDefinition: $columnDefinition);
    }

    public function toString(): string
    {
        $buffer = new StringBuffer('$table->');

        $buffer = match ($this->columnDefinition->dataTypeCast) {
            'int'     => $this->handleTypeInt($buffer),
            'string'  => $this->handleTypeString($buffer),
            'Carbon'  => $this->handleTypeCarbon($buffer),
            'float'   => $this->handleTypeFloat($buffer),
            'boolean' => $this->handleTypeDefault($buffer),
            'array'   => $this->handleTypeDefault($buffer),
            default   => throw new NoCastTypeForDataTypeException($this->columnDefinition->dataTypeCast),
        };

        $buffer->appendIf($this->columnDefinition->nullable, '->nullable()');
        $buffer = $this->addDefault($buffer);
        $buffer->append(';');

        return $buffer->toString();
    }

    private function handleTypeInt(StringBuffer $buffer): StringBuffer
    {
        $method = $this->methodMap[$this->columnDefinition->dataType];

        return $buffer->appendIf(condition: $this->columnDefinition->unsigned, string: 'unsigned')
                      ->appendIf(
                          condition: $this->columnDefinition->unsigned,
                          string   : Str::ucfirst($method),
                          else     : $method
                      )
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )
                      ->appendIf(
                          condition: $this->columnDefinition->autoIncrement,
                          string   : ', autoIncrement: true'
                      )->append(')');
    }

    private function handleTypeString(StringBuffer $buffer): StringBuffer
    {
        return $buffer->append($this->methodMap[$this->columnDefinition->dataType])
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )
                      ->appendIfNotNull(
                          value : $this->columnDefinition->length,
                          string: ', length: ' . $this->columnDefinition->length
                      )->append(')');
    }

    private function handleTypeCarbon(StringBuffer $buffer): StringBuffer
    {
        return $buffer->append($this->methodMap[$this->columnDefinition->dataType])
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )->append(')');
    }

    private function handleTypeDefault(StringBuffer $buffer): StringBuffer
    {
        return $buffer->append($this->methodMap[$this->columnDefinition->dataType])
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )->append(')');
    }

    private function handleTypeFloat(StringBuffer $buffer): StringBuffer
    {
        return $buffer->append($this->methodMap[$this->columnDefinition->dataType])
                      ->append('(')
                      ->appendFormatted(
                          'column: %s::PROPERTY_%s',
                          $this->model,
                          Str::upper($this->columnDefinition->name)
                      )
                      ->appendIfNotNull(
                          value : $this->columnDefinition->length,
                          string: ', total: ' . $this->columnDefinition->length
                      )
                      ->appendIfNotNull(
                          value : $this->columnDefinition->precision,
                          string: ', places: ' . $this->columnDefinition->precision
                      )
                      ->appendIf(
                          condition: $this->columnDefinition->unsigned,
                          string   : ', unsigned: true',
                          else     : ', unsigned: false'
                      )->append(')');
    }

    private function addDefault(StringBuffer $buffer): StringBuffer
    {
        if (!is_null($this->columnDefinition->default)) {
            if ($this->columnDefinition->default === 'CURRENT_TIMESTAMP') {
                $buffer->append('->useCurrent()');
            } elseif (is_bool($this->columnDefinition->default)) {
                $buffer->appendFormatted('->default(%s)', $this->columnDefinition->default === true ? 'true' : 'false');
            } elseif (is_string($this->columnDefinition->default)) {
                $buffer->appendIf(
                    condition: $this->columnDefinition->default === 'NULL',
                    string   : '->default(null)',
                    else     : sprintf('->default(%s)', sprintf("'%s'", $this->columnDefinition->default))
                );
            } else {
                $buffer->appendFormatted('->default(%s)', $this->columnDefinition->default);
            }
        }

        return $buffer;
    }
}
