<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition;

class ModelRequestGenerator extends AbstractGenerator
{
    public const TYPE_STORE  = 'Store';
    public const TYPE_UPDATE = 'Update';

    protected static string $generator = 'request';
    protected string $template  = 'model_request';

    public string $type = '';

    public function __construct(ModelDefinition $modelDefinition, string $type)
    {
        parent::__construct($modelDefinition);
        $this->type = $type;
    }

    protected function beforeWrite(): void
    {
        $this->addUses([
            sprintf(
                '%s\%s',
                call_user_func([ModelGenerator::class, 'namespace']),
                $this->modelDefinition->model
            ),
            sprintf('App\Http\Requests\Validators\%sValidator', $this->modelDefinition->model),
        ]);
        $this->setExtends(config('craftingtable.request.extends'));
    }
}
