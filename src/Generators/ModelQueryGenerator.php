<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelQueryGenerator extends AbstractGenerator
{
    protected static string $generator = 'query';
    protected string $template  = 'model_query';

    protected function beforeWrite(): void
    {
        $this->setExtends(config('craftingtable.query.extends'));
    }

}
