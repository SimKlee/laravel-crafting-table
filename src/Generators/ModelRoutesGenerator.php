<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelRoutesGenerator extends AbstractGenerator
{
    protected static string $generator = 'route';
    protected string $template  = 'model_routes';

    protected function beforeWrite(): void
    {
        $this->addUses([
            \Illuminate\Support\Facades\Route::class,
            sprintf('\App\Http\Controllers\%sController', $this->modelDefinition->model),
        ]);
        $this->addUses([
            \Illuminate\Support\Facades\Route::class,
            sprintf(
                '\App\Http\Controllers\Api\%sController as %sApiController',
                $this->modelDefinition->model,
                $this->modelDefinition->model
            ),
        ]);
        $this->setExtends(config('craftingtable.route.extends'));
    }
}
