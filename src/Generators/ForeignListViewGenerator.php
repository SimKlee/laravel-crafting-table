<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

use ErrorException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;
use SimKlee\LaravelCraftingTable\Exceptions\MissingModelDefinitionKey;
use SimKlee\LaravelCraftingTable\LaravelCraftingTableServiceProvider;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinitionBag;
use SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition;

class ForeignListViewGenerator
{
    private LazyCollection     $stubContent;
    private ModelDefinitionBag $modelDefinitionBag;
    private ModelDefinition    $modelDefinition;
    private RelationDefinition $relationDefinition;
    private array              $replacements = [];
    private array              $columns      = [];

    public function __construct(ModelDefinitionBag $modelDefinitionBag, ModelDefinition $modelDefinition, RelationDefinition $relationDefinition)
    {
        $this->modelDefinitionBag = $modelDefinitionBag;
        $this->modelDefinition    = $modelDefinition;
        $this->relationDefinition = $relationDefinition;

        $this->stubContent = File::lines(
            LaravelCraftingTableServiceProvider::packageResourcePath('stubs/foreign_list.stub')
        );
        $this->getColumns();

        $foreignModelDefinition = $this->modelDefinitionBag->get($this->relationDefinition->model);

        $this->replacements = [
            '{{ model:name }}'         => $modelDefinition->model,
            '{{ model:id }}'           => $modelDefinition->uuid ? 'uuid' : 'id',
            '{{ model:snake }}'        => $modelDefinition->getModelVarName(snake: true),
            '{{ model:var }}'          => $modelDefinition->getModelVarName(),
            '{{ model:vars }}'         => Str::snake($modelDefinition->table),
            '{{ model:table }}'        => $modelDefinition->table,
            '{{ title }}'              => sprintf("{{ trans('%s.titles.%s') }}", $modelDefinition->table, $this->relationDefinition->name),
            '{{ foreignModel:name }}'  => $foreignModelDefinition->model,
            '{{ foreignModel:snake }}'  => $foreignModelDefinition->getModelVarName(snake: true),
            '{{ foreignModel:var }}'   => $foreignModelDefinition->getModelVarName(),
            '{{ foreignModel:vars }}'  => Str::camel($foreignModelDefinition->table),
            '{{ foreignModel:table }}' => $foreignModelDefinition->table,
        ];
    }

    private function getColumns()
    {
        $columns = config(sprintf('models.%s.views.%s', $this->modelDefinition->model, $this->relationDefinition->name));

        if (is_null($columns) || count($columns) === 0) {
            $columns = $this->modelDefinition
                ->getFilteredColumns(['id', 'uuid', 'created_at', 'updated_at', 'deleted_at'])
                ->map(function (ColumnDefinition $columnDefinition) {
                    return $columnDefinition->name;
                })->toArray();
        }

        $this->columns = $columns;
    }

    private function processContent(): string
    {
        $content = [];
        $loop    = false;
        foreach ($this->stubContent as $line) {

            if (Str::contains(haystack: $line, needles: '{{ foreachColumnDefinition }}')) {
                $loop = true;
                continue;
            }

            if (Str::contains(haystack: $line, needles: '{{ endForeachColumnDefinition }}')) {
                $loop = false;
                continue;
            }

            if ($loop) {
                $content = array_merge($content, $this->processColumnDefinitionsLoop($line));
            } else {
                $content[] = $this->replace($line, $this->replacements);
            }
        }

        return implode(separator: PHP_EOL, array: $content);
    }

    public function write(): string
    {
        $path = sprintf('views/%s', $this->modelDefinition->table);
        if (File::isDirectory(resource_path($path)) === false) {
            File::makeDirectory(path: resource_path($path), mode: 0755, recursive: true);
        }

        File::put(
            path    : resource_path($path . sprintf('/%s_list.blade.php', Str::snake($this->relationDefinition->name))),
            contents: $this->processContent()
        );

        return sprintf('resources/views/%s/%s_list.blade.php', $this->modelDefinition->table, Str::snake($this->relationDefinition->name));
    }

    private function processColumnDefinitionsLoop(string $line): array
    {
        $lines = [];
        /** @var ColumnDefinition $columnDefinition */
        foreach ($this->modelDefinition->columns as $columnDefinition) {
            if (!in_array(needle: $columnDefinition->name, haystack: $this->columns) && count($this->columns) > 0) {
                continue;
            }

            $lines[] = $this->replace($line, array_merge(
                $this->getColumnReplacements($columnDefinition),
                $this->replacements
            ));;
        }

        return $lines;
    }

    private function getColumnReplacements(ColumnDefinition $columnDefinition): array
    {
        $lookup = '';
        if ($columnDefinition->foreignKey) {
            try {
                $lookup = sprintf('lookup="true" lookup-label="%s"', $this->modelDefinitionBag->get($columnDefinition->foreignKeyModel)->lookup['label']);
            } catch (ErrorException $e) {
                throw new MissingModelDefinitionKey(sprintf('Missing key "label" for %s', $columnDefinition->foreignKeyModel));
            }
        } elseif (count($this->getValues($columnDefinition->name)) > 0) {
            $lookup = 'lookup="true"';
        }

        return [
            // @TODO: refac (remove)
            '{{ column:name }}'         => $this->relationDefinition->name === 'index' && $columnDefinition->foreignKey
                ? sprintf(
                    '%s->%s',
                    Str::camel(Str::replace('_id', '', $columnDefinition->name)),
                    $this->modelDefinitionBag->get($columnDefinition->foreignKeyModel)->lookup['label']
                )
                : $columnDefinition->name,
            '{{ column:translation }}'  => $columnDefinition->name,
            '{{ column:dataTypeCast }}' => $columnDefinition->dataTypeCast,
            '{{ column:length }}'       => $columnDefinition->length,
            '{{ column:nullable }}'     => $columnDefinition->nullable,
            '{{ lookup }}'              => $lookup,
        ];
    }

    public function addReplacement(string $search, string $replace): void
    {
        $this->replacements[$search] = $replace;
    }

    private function replace(string $string, array $replacements): string
    {
        return Str::replace(
            search : array_keys($replacements),
            replace: array_values($replacements),
            subject: $string
        );
    }

    private function getValues(string $colum): array
    {
        if (isset($this->modelDefinition->values[$colum])) {
            return $this->modelDefinition->values[$colum];
        }

        return [];
    }
}
