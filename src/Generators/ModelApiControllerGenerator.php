<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelApiControllerGenerator extends AbstractGenerator
{
    protected static string $generator = 'api_controller';
    protected string        $template  = 'model_api_controller';

    protected function beforeWrite(): void
    {
        $this->addUses([
            \App\Http\Controllers\Controller::class,
            \Illuminate\Http\Resources\Json\AnonymousResourceCollection::class,
            \Illuminate\Http\Response::class,
            sprintf(
                '%s\%sResource',
                call_user_func([ModelResourceGenerator::class, 'namespace']),
                $this->modelDefinition->model
            ),
            sprintf(
                '%s\%sStoreRequest',
                call_user_func([ModelRequestGenerator::class, 'namespace']),
                $this->modelDefinition->model
            ),
            sprintf(
                '%s\%sUpdateRequest',
                call_user_func([ModelRequestGenerator::class, 'namespace']),
                $this->modelDefinition->model
            ),
            sprintf(
                '%s\%s',
                call_user_func([ModelGenerator::class, 'namespace']),
                $this->modelDefinition->model
            ),
        ]);

    }

}
