<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelFactoryGenerator extends AbstractGenerator
{
    protected static string $generator = 'factory';
    protected string        $template  = 'model_factory';

    protected function beforeWrite(): void
    {

    }
}
