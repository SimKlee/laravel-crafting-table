<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelTestGenerator extends AbstractGenerator
{
    protected static string $generator = 'model_test';
    protected string        $template  = 'model_test';

    protected function beforeWrite(): void
    {
        $this->addUses([
            \Illuminate\Support\Collection::class,
            sprintf('%s\%s', ModelGenerator::namespace(), $this->modelDefinition->model),
        ]);
        $this->setExtends(config('craftingtable.model_test.extends'));
    }

}
