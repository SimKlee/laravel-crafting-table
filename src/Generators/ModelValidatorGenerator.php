<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

class ModelValidatorGenerator extends AbstractGenerator
{
    protected static string $generator = 'validator';
    protected string $template  = 'model_validator';

    protected function beforeWrite(): void
    {
        $this->addUse(sprintf('App\Models\%s', $this->modelDefinition->model));
    }
}
