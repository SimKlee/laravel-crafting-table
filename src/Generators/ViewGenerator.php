<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable\Generators;

use ErrorException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;
use SimKlee\LaravelCraftingTable\Exceptions\MissingModelDefinitionKey;
use SimKlee\LaravelCraftingTable\LaravelCraftingTableServiceProvider;
use SimKlee\LaravelCraftingTable\Models\Definitions\ColumnDefinition;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinition;
use SimKlee\LaravelCraftingTable\Models\Definitions\ModelDefinitionBag;
use SimKlee\LaravelCraftingTable\Models\Definitions\RelationDefinition;

class ViewGenerator
{
    private string             $stub;
    private LazyCollection     $stubContent;
    private ModelDefinitionBag $modelDefinitionBag;
    private ModelDefinition    $modelDefinition;
    private array              $replacements = [];
    private array              $columns      = [];

    public function __construct(ModelDefinitionBag $modelDefinitionBag, ModelDefinition $modelDefinition, string $stub)
    {
        $this->modelDefinitionBag = $modelDefinitionBag;
        $this->modelDefinition    = $modelDefinition;
        $this->stub               = $stub;
        $this->stubContent        = File::lines(
            LaravelCraftingTableServiceProvider::packageResourcePath(sprintf('stubs/%s.stub', $this->stub))
        );
        $this->getColumns();

        $this->replacements = [
            '{{ model:name }}'       => $modelDefinition->model,
            '{{ model:id }}'         => $modelDefinition->uuid ? 'uuid' : 'id',
            '{{ model:snake }}'      => $modelDefinition->getModelVarName(snake: true),
            '{{ model:var }}'        => $modelDefinition->getModelVarName(),
            '{{ model:vars }}'       => Str::snake($modelDefinition->table),
            '{{ model:table }}'      => $modelDefinition->table,
            '{{ title }}'            => sprintf("{{ trans('%s.titles.%s') }}", $modelDefinition->table, $this->stub),
            '{{ $foreignKeyLists }}' => $this->getForeignKeyLists(),
        ];
    }

    private function getColumns()
    {
        $columns = config(sprintf('models.%s.views.%s', $this->modelDefinition->model, $this->stub));

        if (is_null($columns) || count($columns) === 0) {
            $columns = $this->modelDefinition
                ->getFilteredColumns(['id', 'uuid', 'created_at', 'updated_at', 'deleted_at'])
                ->map(function (ColumnDefinition $columnDefinition) {
                    return $columnDefinition->name;
                })->toArray();
        }

        $this->columns = $columns;
    }

    private function processContent(): string
    {
        $content = [];
        $loop    = false;
        foreach ($this->stubContent as $line) {

            if (Str::contains(haystack: $line, needles: '{{ foreachColumnDefinition }}')) {
                $loop = true;
                continue;
            }

            if (Str::contains(haystack: $line, needles: '{{ endForeachColumnDefinition }}')) {
                $loop = false;
                continue;
            }

            if ($loop) {
                $content = array_merge($content, $this->processColumnDefinitionsLoop($line));
            } else {
                $content[] = $this->replace($line, $this->replacements);
            }
        }

        return implode(separator: PHP_EOL, array: $content);
    }

    public function write(): string
    {
        $path = sprintf('views/%s', $this->modelDefinition->table);
        if (File::isDirectory(resource_path($path)) === false) {
            File::makeDirectory(path: resource_path($path), mode: 0755, recursive: true);
        }

        File::put(
            path    : resource_path($path . sprintf('/%s.blade.php', $this->stub)),
            contents: $this->processContent()
        );

        return sprintf('resources/views/%s/%s.blade.php', $this->modelDefinition->table, $this->stub);
    }

    private function processColumnDefinitionsLoop(string $line): array
    {
        $lines = [];
        /** @var ColumnDefinition $columnDefinition */
        foreach ($this->modelDefinition->columns as $columnDefinition) {
            if (!in_array(needle: $columnDefinition->name, haystack: $this->columns) && count($this->columns) > 0) {
                continue;
            }

            $lines[] = $this->replace($line, array_merge(
                $this->getColumnReplacements($columnDefinition),
                $this->replacements
            ));;
        }

        return $lines;
    }

    private function getColumnReplacements(ColumnDefinition $columnDefinition): array
    {
        $lookup = '';
        if ($columnDefinition->foreignKey) {
            $foreignModelDefinition = $this->modelDefinitionBag->get($columnDefinition->foreignKeyModel);
            try {
                $lookup = sprintf(
                    'lookup="true" lookup-label="%s" lookup-default="{{ request()->get(\'%s\') }}"',
                    $foreignModelDefinition->lookup['label'],
                    Str::replace('_id', '', $columnDefinition->name)
                );
            } catch (ErrorException $e) {
                throw new MissingModelDefinitionKey(sprintf('Missing key "label" for %s', $columnDefinition->foreignKeyModel));
            }
        } elseif (count($this->getValues($columnDefinition->name)) > 0) {
            $lookup = 'lookup="true"';
        }

        return [
            '{{ column:name }}'         => $this->stub === 'index' && $columnDefinition->foreignKey
                ? sprintf(
                    '%s->%s',
                    Str::camel(Str::replace('_id', '', $columnDefinition->name)),
                    $this->modelDefinitionBag->get($columnDefinition->foreignKeyModel)->lookup['label']
                )
                : $columnDefinition->name,
            '{{ column:translation }}'  => $columnDefinition->name,
            '{{ column:dataTypeCast }}' => $columnDefinition->dataTypeCast,
            '{{ column:length }}'       => $columnDefinition->length,
            '{{ column:nullable }}'     => $columnDefinition->nullable,
            '{{ lookup }}'              => $lookup,
        ];
    }

    public function addReplacement(string $search, string $replace): void
    {
        $this->replacements[$search] = $replace;
    }

    private function replace(string $string, array $replacements): string
    {
        return Str::replace(
            search : array_keys($replacements),
            replace: array_values($replacements),
            subject: $string
        );
    }

    private function getValues(string $colum): array
    {
        if (isset($this->modelDefinition->values[$colum])) {
            return $this->modelDefinition->values[$colum];
        }

        return [];
    }

    private function getForeignKeyLists(): string
    {
        if (in_array($this->stub, ['show', 'edit'])) {
            return $this->modelDefinition->relations->filter(function (RelationDefinition $relationDefinition) {
                return in_array($relationDefinition->type, config('craftingtable.view_relations.relations', [RelationDefinition::TYPE_HAS_MANY]));
            })->map(function (RelationDefinition $relationDefinition) {
                $foreignModelDefinition = $this->modelDefinitionBag->get($relationDefinition->model);
                return sprintf(
                    "\t@include('%s.%s_list', ['%s' => $%s->%s, 'actions' => %s])",
                    $this->modelDefinition->table,
                    $foreignModelDefinition->table,
                    Str::camel($foreignModelDefinition->table),
                    $this->modelDefinition->getModelVarName(snake: false),
                    $relationDefinition->name,
                    $this->stub === 'show' ? 'false' : 'true'
                );
            })->implode(PHP_EOL);
        }

        return '';
    }
}
