<?php

declare(strict_types=1);

namespace SimKlee\LaravelCraftingTable;

use Illuminate\Support\ServiceProvider;
use SimKlee\LaravelCraftingTable\Console\Commands\BladeGenerator;
use SimKlee\LaravelCraftingTable\Console\Commands\InstallCommand;
use SimKlee\LaravelCraftingTable\Console\Commands\ModelCrafterCommand;
use SimKlee\LaravelCraftingTable\Console\Commands\ModelDeleteCommand;

class LaravelCraftingTableServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {

            $this->commands($this->getCommands());

            $this->publishes([
                __DIR__ . '/../config/models.php'        => config_path('models.php'),
                __DIR__ . '/../config/craftingtable.php' => config_path('craftingtable.php'),
            ], 'config');
        }

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'crafting-table');


    }

    public function register(): void
    {
        #$this->mergeConfigFrom(__DIR__ . '/../config/models.php', 'models');
        $this->mergeConfigFrom(__DIR__ . '/../config/craftingtable.php', 'craftingtable');
    }

    private function getCommands(): array
    {
        return [
            InstallCommand::class,
            ModelCrafterCommand::class,
            ModelDeleteCommand::class,
            BladeGenerator::class,
        ];
    }

    public static function packagePath(string $file = null): string
    {
        $path = __DIR__ . '/..';
        
        if (!is_null($file)) {
            $path .= '.' . $file;
        }
        
        return $path;
    }

    public static function packageResourcePath(string $file = null): string
    {
        $path = __DIR__ . '/../resources';
        if (!is_null($file)) {
            $path .= '/' . $file;
        }
        return $path;
    }
}
